@extends('admin.main')

@section('title', 'Admin Dashboard')

@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light portlet-fit portlet-datatable ">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-layers font-green"></i>
                    <span class="caption-subject font-green sbold uppercase">Trips</span>
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="dataTable">
                    <thead>
                    <tr>
                        <th> # </th>
                        <th> Tprip Type </th>
                        <th> Name </th>
                        <th> Email </th>
                        <th> Status </th>
                        <th> Create at </th>
                        <th> More detals </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($trips as $trip)
                        <tr class="odd gradeX"  @if($trip->flexible_checked)
                                                    style="color: #0000ff"
                                                @endif>
                            <td> {{ $loop->iteration }}</td>
                            <td> {{ $trip->trip_type }} </td>
                            <td>{{ $trip->client->first_name }}</td>
                            <td>
                                <a href="mailto:{{ $trip->client->user->email }}">{{ $trip->client->user->email }}</a>
                            </td>
                            <td>
                                <div class="form-inline">
                                    <select name="status" class="form-control select-status" data-trip-id="{{$trip->id}}">
                                        <option {{($trip->status == 'New')?'selected':''}} value="New">New</option>
                                        <option {{($trip->status == 'In Process')?'selected':''}} value="In Process">In Process</option>
                                        <option {{($trip->status == 'Closed')?'selected':''}} value="Closed">Closed</option>
                                    </select>
                               </div>

                            </td>
                            <td>
                                <span>{{ $trip->created_at->format('m-d-Y') }}</span>
                            </td>
                            <td>
                                <span> <a href="{{ asset('/admin/trip/' . $trip->id) }}">Show</a></span>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
    <script src="{{ asset('/assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            table = $('#dataTable').DataTable({
                "pageLength": 50
            });
        });

        $('.select-status').on('change', function()
        {
            var status = $(this).val();
            var trip_id = $(this).attr("data-trip-id");
            var token = "{{ csrf_token() }}";
            var url = "{{ route('admin.trip.update.status') }}";
            $.ajax({
                type: "POST",
                url: url,
                data: {status: status, trip_id: trip_id, _token:token},
                success: function()
                {
                    $('#status-saved-' + trip_id).show().css({
                        "opacity" : "1",
                        "transition" : "opacity 2s ease-in-out"
                    });
                }
            });
        });
    </script>
@endsection

@section('stylesheets')
    <link href="{{ asset('/assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
@endsection