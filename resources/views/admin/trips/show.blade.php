@extends('admin.main')

@section('title', 'Admin Dashboard')

@section('content')

<div class="row">

    <div class="col-md-8">
        <div class="portlet box blue-hoki">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa icon-social-dribbble"></i>Direction Info [ Trip ID - {{ $trip->id }} ] </div>
            </div>
            <div class="portlet-body">
                <div class="table-scrollable">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th> # </th>
                            <th> Trip Type </th>
                            <th> From Code</th>
                            <th> From City </th>
                            <th> From Country </th>
                            <th> To Code</th>
                            <th> To City </th>
                            <th> To Country </th>

                            @if($trip->flexible_checked)
                                <th> Flexible Date </th>
                                <th> Flexible Period </th>
                            @else
                                <th> Departure Date </th>
                                @if(!empty($trip->directions[0]->return_date))
                                    <th> Return Date </th>
                                @endif
                            @endif

                            <th> Adults </th>
                            <th> Class </th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($trip->directions as $direction)
                            <tr>
                                <td> {{$loop->iteration}} </td>
                                <td> {{ $direction->trip_type }} </td>
                                <td> {{ $direction->from }} </td>
                                <td> {{ $locality_directions['from'][$loop->iteration - 1]['city'] }} </td>
                                <td> {{ $locality_directions['from'][$loop->iteration - 1]['country'] }} </td>
                                <td> {{ $direction->to }} </td>
                                <td> {{ $locality_directions['to'][$loop->iteration - 1]['city'] }} </td>
                                <td> {{ $locality_directions['to'][$loop->iteration - 1]['country'] }} </td>

                                @if($trip->flexible_checked)
                                    <td> {{ $trip->flexible_date }} </td>
                                    <td> {{ $trip->flexible_period }} </td>
                                @else
                                    <td> {{ \Carbon\Carbon::createFromFormat('Y-m-d', $direction->departure_date)->format('m-d-Y') }} </td>
                                    @if(!empty($direction->return_date))
                                        <td> {{ \Carbon\Carbon::createFromFormat('Y-m-d', $direction->return_date)->format('m-d-Y') }} </td>
                                    {{--@else--}}
                                        {{--<td> {{ $direction->return_date }} </td>--}}
                                    @endif
                                @endif

                                <td> {{ $direction->adults }} </td>
                                <td> {{ $direction->class }} </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="portlet box blue-hoki">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa icon-user"></i>Contact Info [ Client ID - {{ $trip->client->id }}]</div>
            </div>
            <div class="portlet-body">
                <div class="table-scrollable">
                    <table class="table table-hover">
                        <tr>
                            <th> First Name </th>
                            <td> {{ $trip->client->first_name }} </td>
                        </tr>
                        <tr>
                            <th> Last Name </th>
                            <td> {{ $trip->client->last_name }} </td>
                        </tr>
                        <tr>
                            <th> Email </th>
                            <td> {{ $trip->client->user->email }} </td>
                        </tr>
                        <tr>
                            <th> Mobile phone </th>
                            <td> {{ $trip->client->mobile_phone_number }} </td>
                        </tr>
                        <tr>
                            <th> Bussines phone </th>
                            <td> {{ $trip->client->business_phone_number }} </td>
                        </tr>
                        <tr>
                            <th> Country </th>
                            <td> {{ $trip->client->country }} </td>
                        </tr>
                        <tr>
                            <th> City </th>
                            <td> {{ $trip->client->city_town_department }} </td>
                        </tr>
                        <tr>
                            <th> ZIP </th>
                            <td> {{ $trip->client->zip_personal_code }} </td>
                        </tr>
                        <tr>
                            <th> Street Address 1 </th>
                            <td> {{ $trip->client->street_address_line_1 }} </td>
                        </tr>
                        <tr>
                            <th> Street Address 2 </th>
                            <td> {{ $trip->client->street_address_line_2 }} </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('javascript')

@endsection

@section('stylesheets')

@endsection