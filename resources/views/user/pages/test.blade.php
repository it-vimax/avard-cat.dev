@extends('user.main')

@section('title', 'Main')
@section('description', 'Service for spending points.')

@section('content')

<!-- Intro	================================================== -->
<section id="hero" class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="herotext">

                    <script language="JavaScript">
                        function openPage(){
                            var h = 650;
                            var w = 450;
                            var y = window.top.outerHeight / 2 + window.top.screenY - ( h / 2);
                            var x = window.top.outerWidth / 2 + window.top.screenX - ( w / 2);
                            window.open('https://awardwallet.com/m/connections/approve/zhomdqxmie/2', '', 'toolbar=no, location=0, directories=no, status=no, menubar=no, scrollbars=no, resizable=0, copyhistory=no, width=' + w + ', height=' + h + ', top=' + y + ', left=' + x);
                        }
                    </script>
                    <a href="#" class="btn btn-block btn-primary" onclick="openPage()">open</a>

                </div>
            </div>
            <div class="col-md-7">
            </div>
        </div>
    </div>
</section>
<!--======================= FORM ===========================-->
<section id="form-section" class="parallax section" style="background-image: url('/img/1.jpg');">
    <div class="wrapsection">
        <div class="parallax-overlay" style="background-color: #0023f2;opacity:0.9;"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- Title -->
                    <div class="maintitle">
                        <h3 class="section-title">TELL US WHERE YOU WANT TO GO</h3>
                        <p class="lead">
                            AwardCat will do the rest!
                        </p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-md-offset-3">
                    <div class="service-box wow zoomIn" data-wow-duration="1.5s" data-wow-delay="0.1s">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#round_trip">Round-trip</a></li>
                            <li><a data-toggle="tab" href="#one_way">One-way</a></li>
                            <li><a data-toggle="tab" href="#multi_city">Multi-city</a></li>
                        </ul>

                        <div class="tab-content">

                            <!-- ROUND-TRIP TAB -->
                            <div id="round_trip" class="tab-pane fade in active">
                                {!! Form::open(['route' => 'roundTripStepTwo', 'method'=>'POST', 'class'=>'trip-form', 'id'=>'round_trip_form']) !!}

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group control-group">
                                            <label for="select_from">From</label>
                                            <select id="select_from" name="select_from" class="form-control airports selectize-form-search" required></select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="select_to">To</label>
                                            <select id="select_to" name="select_to" class="form-control airports selectize-form-search" required></select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="flexible" id="flexible" value="1"> My dates are flexible</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row" id="round-trip-dates">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="departure_date">Departure date</label>
                                            <div class="input-group date date-picker" data-date-format="mm-dd-yyyy">
                                                <input type="text" class="form-control" readonly name="departure_date">
                                                <span class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="return_date">Return date</label>
                                            <div class="input-group date date-picker" data-date-format="mm-dd-yyyy">
                                                <input type="text" class="form-control" readonly name="return_date">
                                                <span class="input-group-btn">
                                                    <button class="btn default" type="button">
                                                        <i class="fa fa-calendar"></i>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row" id="round-trip-flexible-dates" style="display: none;">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="flexible_date">Date</label>
                                            <select name="flexible_date" class="form-control">
                                                @for ($i = 0; $i < 11; $i++)
                                                    <option value="{{Carbon\Carbon::now()->addMonth($i)->format('m')}}">{{Carbon\Carbon::now()->addMonth($i)->format('F Y')}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="period">Period</label>
                                            <select name="period" class="form-control">
                                            <option value="1" >1 day</option>
                                            <option value="2" >2 days</option>
                                            <option value="3" >3 days</option>
                                            <option value="4" >4 days</option>
                                            <option value="5" >5 days</option>
                                            <option value="6" selected=selected>6 days</option>
                                            <option value="7" >7 days</option>
                                            <option value="8" >8 days</option>
                                            <option value="9" >9 days</option>
                                            <option value="10" >10 days</option>
                                            <option value="11" >11 days</option>
                                            <option value="12" >12 days</option>
                                            <option value="13" >13 days</option>
                                            <option value="14" >14 days</option>
                                            <option value="15" >15 days</option>
                                            <option value="16" >16 days</option>
                                            <option value="17" >17 days</option>
                                            <option value="18" >18 days</option>
                                            <option value="19" >19 days</option>
                                            <option value="20" >20 days</option>
                                            <option value="21" >21 days</option>
                                            <option value="22" >22 days</option>
                                            <option value="23" >23 days</option>
                                            <option value="24" >24 days</option>
                                            <option value="25" >25 days</option>
                                            <option value="30">30 days</option>
                                            <option value="60">60 days</option>
                                            <option value="90">90 days</option>
                                            <option value="120">120 days</option>
                                            <option value="180">180 days</option>
                                        </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="adults">Adults</label>
                                            <select class="form-control" name="adults">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="class">Class</label>
                                            <select class="form-control" name="class">
                                                <option value="business">Business</option>
                                                <option value="first">First</option>
                                                <option value="economy">Economy</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-block btn-primary">Find Flights</button>
                                    </div>
                                </div>

                                {!! Form::close() !!}
                            </div>
                            <!-- END ROUND-TRIP TAB -->


                            <!-- ONE-WAY TAB -->
                            <div id="one_way" class="tab-pane fade in">
                                {!! Form::open(['route' => 'oneWayStepTwo', 'class'=>'trip-form', 'id'=>'one_way_form']) !!}

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group control-group">
                                            <label for="one_way_select_from">From</label>
                                            <select id="one_way_select_from" name="one_way_select_from" class="form-control airports selectize-form-search" required></select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="one_way_select_to">To</label>
                                            <select id="one_way_select_to" name="one_way_select_to" class="form-control airports selectize-form-search" required></select>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="departure_date">Departure date</label>
                                            <div class="input-group date date-picker" data-date-format="mm-dd-yyyy">
                                                <input type="text" class="form-control" readonly name="departure_date">
                                                <span class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="people">Adults</label>
                                            <select class="form-control" name="adults">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="people">Class</label>
                                            <select class="form-control" name="class">
                                                <option value="business">Business</option>
                                                <option value="first">First</option>
                                                <option value="economy">Economy</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-block btn-primary">Find Flights</button>
                                    </div>
                                </div>

                                {!! Form::close() !!}
                            </div>
                            <!-- END ONE-WAY TAB -->


                            <!-- MULTY CITY TAB -->
                            <div id="multi_city" class="tab-pane fade in">
                                <h3>It's Multy city tab. There should be some explanation...</h3>
                                {!! Form::open(['route' => 'multiCityStepTwo', 'class'=>'trip-form', 'id'=>'multi_city_form']) !!}

                                    <h5>Destination 1</h5>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group control-group">
                                                <label for="multy_city_select_from0">From</label>
                                                <select id="multy_city_select_from0" name="multy_city_select_from[]" class="form-control airports selectize-form-search" required></select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="multy_city_select_to0">To</label>
                                                <select id="multy_city_select_to0" name="multy_city_select_to[]" class="form-control airports selectize-form-search" required></select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="departure_date">Departure date</label>
                                                <div class="input-group date date-picker" data-date-format="mm-dd-yyyy">
                                                    <input type="text" class="form-control" readonly name="departure_date[]">
                                                    <span class="input-group-btn">
                                                        <button class="btn default" type="button">
                                                            <i class="fa fa-calendar"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>

                                    <h5>Destination 2</h5>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group control-group">
                                                <label for="multy_city_select_from1">From</label>
                                                <select id="multy_city_select_from1" name="multy_city_select_from[]" class="form-control airports selectize-form-search" required></select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="multy_city_select_to1">To</label>
                                                <select id="multy_city_select_to1" name="multy_city_select_to[]" class="form-control airports selectize-form-search" required></select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="departure_date">Departure date</label>
                                                <div class="input-group date date-picker" data-date-format="mm-dd-yyyy">
                                                    <input type="text" class="form-control" readonly name="departure_date[]">
                                                    <span class="input-group-btn">
                                                        <button class="btn default" type="button">
                                                            <i class="fa fa-calendar"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>

                                    <h5>Destination 3</h5>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group control-group">
                                                <label for="multy_city_select_from2">From</label>
                                                <select id="multy_city_select_from2" name="multy_city_select_from[]" class="form-control airports selectize-form-search" required></select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="multy_city_select_to2">To</label>
                                                <select id="multy_city_select_to2" name="multy_city_select_to[]" class="form-control airports selectize-form-search" required></select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="departure_date">Departure date</label>
                                                <div class="input-group date date-picker" data-date-format="mm-dd-yyyy">
                                                    <input type="text" class="form-control" readonly name="departure_date[]">
                                                    <span class="input-group-btn">
                                                        <button class="btn default" type="button">
                                                            <i class="fa fa-calendar"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>

                                <div id="destinations"></div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="adults">Adults</label>
                                            <select class="form-control" name="adults">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="class">Class</label>
                                            <select class="form-control" name="class">
                                                <option value="business">Business</option>
                                                <option value="first">First</option>
                                                <option value="economy">Economy</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="button" id="remove_destination_btn" class="btn btn-primary btn-xs btn-block">Remove destination</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" id="add_another_destination_btn" class="btn btn-primary btn-xs btn-block">Add another destin.</button>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-block btn-primary">Find Flights</button>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                            <!-- END MULTY CITY TAB -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('javascript')

    <script src="{{ asset('js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{ asset('assets/selectize.js/dist/js/standalone/selectize.js')}}"></script>

    <script type="text/javascript">


        //SELECT FROM
        $('.selectize-form-search').selectize({
            valueField: 'code',
            labelField: 'city',
            selectOnTab: 'true',
            searchField: ['code', 'name', 'city'],
            optgroupValueField: 'city_id',
            optgroupField: 'city_id',
            sortField: 'sort',
            create: false,
            render: {
                option: function(item, escape) {
                    var airports = [];
                    for (var i = 0, n = item.length; i < n; i++) {
                        airports.push('<span>' + escape(item.city) +' (' + escape(item.code) +' ' + escape(item.name) +')</span>');
                    }

                    if(item.name == 'All Airports'){
                        var str = '<div>' +
                            '<span style="font-weight:900;">' + escape(item.city)+', '+ item.country +' (' + escape(item.code) +' ' + escape(item.name) +')</span>' +
                            '</div>';
                    }else{
                        var str = '<div>' +
                            '<span>' + escape(item.city)+', '+ item.country +' (' + escape(item.code) +' ' + escape(item.name) +')</span>' +
                            '</div>';
                    }

                    return str;
                }
            },
            load: function(query, callback) {
                if (!query.length) return callback();

                var url = "{{route('main.search')}}";
                var token = "{{ csrf_token() }}";

                $.ajax({
                    url: url,
                    type: 'POST',
                    data: {q: query,_token:token,
                    },
                    error: function() {
                        callback();
                    },
                    success: function(res) {
                        console.log(res);
                        callback(res);
                    }
                });
            }
        });

        //Flexible dates
        $('#flexible').on('change', function () {
            $('#round-trip-dates').toggle();
            $('#round-trip-flexible-dates').toggle();
        });


        var FormValidation = function () {


            var roundTripFormValidation = function() {

                var round_trip_form = $('#round_trip_form');
                var error = $('.alert-danger', round_trip_form);
                var success = $('.alert-success', round_trip_form);

                round_trip_form.validate({
                    errorElement: 'span', //default input error message container
                    errorClass: 'help-block help-block-error', // default input error message class
                    focusInvalid: false, // do not focus the last invalid input
                    ignore: "", // validate all fields including form hidden input
                    rules: {
                        from: {
                            required: true
                        },
                        to: {
                            required: true
                        },

                    },

                    messages: { // custom messages for radio buttons and checkboxes
                        oneway: {
                            required: "Please select a Trip type"
                        },
                        service: {
                            required: "Please select  at least 2 types of Service",
                            minlength: jQuery.validator.format("Please select  at least {0} types of Service")
                        }
                    },

                    errorPlacement: function (error, element) { // render error placement for each input typeW
                        if (element.parents('.mt-radio-list').size() > 0 || element.parents('.mt-checkbox-list').size() > 0) {
                            if (element.parents('.mt-radio-list').size() > 0) {
                                error.appendTo(element.parents('.mt-radio-list')[0]);
                            }
                            if (element.parents('.mt-checkbox-list').size() > 0) {
                                error.appendTo(element.parents('.mt-checkbox-list')[0]);
                            }
                        } else if (element.parents('.mt-radio-inline').size() > 0 || element.parents('.mt-checkbox-inline').size() > 0) {
                            if (element.parents('.mt-radio-inline').size() > 0) {
                                error.appendTo(element.parents('.mt-radio-inline')[0]);
                            }
                            if (element.parents('.mt-checkbox-inline').size() > 0) {
                                error.appendTo(element.parents('.mt-checkbox-inline')[0]);
                            }
                        } else if (element.parent(".input-group").size() > 0) {
                            error.insertAfter(element.parent(".input-group"));
                        } else if (element.attr("data-error-container")) {
                            error.appendTo(element.attr("data-error-container"));
                        } else {
                            error.insertAfter(element); // for other inputs, just perform default behavior
                        }
                    },

                    invalidHandler: function (event, validator) { //display error alert on form submit
                        success.hide();
                        error.show();
                        //App.scrollTo(error3, -200);
                    },

                    highlight: function (element) { // hightlight error inputs
                        $(element)
                            .closest('.form-group').addClass('has-error'); // set error class to the control group
                    },

                    unhighlight: function (element) { // revert the change done by hightlight
                        $(element)
                            .closest('.form-group').removeClass('has-error'); // set error class to the control group
                    },

                    success: function (label) {
                        label
                            .closest('.form-group').removeClass('has-error'); // set success class to the control group
                    },

                    submitHandler: function (form) {
                        success.show();
                        error.hide();
                        form[0].submit(); // submit the form
                    }

                });

                //initialize datepicker
                $('.date-picker').datepicker({
                    autoclose: true,
                    startDate: 'd',
                });
                $('.date-picker .form-control').change(function() {
                    round_trip_form.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
                })
            }

            var oneWayFormValidation = function() {

                var one_way_form = $('#one_way_form');
                var error = $('.alert-danger', one_way_form);
                var success = $('.alert-success', one_way_form);

                one_way_form.validate({
                    errorElement: 'span', //default input error message container
                    errorClass: 'help-block help-block-error', // default input error message class
                    focusInvalid: false, // do not focus the last invalid input
                    ignore: "", // validate all fields including form hidden input
                    rules: {
                        from: {
                            required: true
                        },
                        to: {
                            required: true
                        },

                    },

                    messages: { // custom messages for radio buttons and checkboxes
                        oneway: {
                            required: "Please select a Trip type"
                        },
                    },

                    errorPlacement: function (error, element) { // render error placement for each input typeW
                        if (element.parents('.mt-radio-list').size() > 0 || element.parents('.mt-checkbox-list').size() > 0) {
                            if (element.parents('.mt-radio-list').size() > 0) {
                                error.appendTo(element.parents('.mt-radio-list')[0]);
                            }
                            if (element.parents('.mt-checkbox-list').size() > 0) {
                                error.appendTo(element.parents('.mt-checkbox-list')[0]);
                            }
                        } else if (element.parents('.mt-radio-inline').size() > 0 || element.parents('.mt-checkbox-inline').size() > 0) {
                            if (element.parents('.mt-radio-inline').size() > 0) {
                                error.appendTo(element.parents('.mt-radio-inline')[0]);
                            }
                            if (element.parents('.mt-checkbox-inline').size() > 0) {
                                error.appendTo(element.parents('.mt-checkbox-inline')[0]);
                            }
                        } else if (element.parent(".input-group").size() > 0) {
                            error.insertAfter(element.parent(".input-group"));
                        } else if (element.attr("data-error-container")) {
                            error.appendTo(element.attr("data-error-container"));
                        } else {
                            error.insertAfter(element); // for other inputs, just perform default behavior
                        }
                    },

                    invalidHandler: function (event, validator) { //display error alert on form submit
                        success.hide();
                        error.show();
                        //App.scrollTo(error3, -200);
                    },

                    highlight: function (element) { // hightlight error inputs
                        $(element)
                            .closest('.form-group').addClass('has-error'); // set error class to the control group
                    },

                    unhighlight: function (element) { // revert the change done by hightlight
                        $(element)
                            .closest('.form-group').removeClass('has-error'); // set error class to the control group
                    },

                    success: function (label) {
                        label
                            .closest('.form-group').removeClass('has-error'); // set success class to the control group
                    },

                    submitHandler: function (form) {
                        success.show();
                        error.hide();
                        form[0].submit(); // submit the form
                    }

                });

//            initialize datepicker
            $('.date-picker').datepicker({
                autoclose: true,
                startDate: 'd',
            });

                $('.date-picker .form-control').change(function() {
                    one_way_form.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
                })
            }

            var multiCityValidation = function(){
                var multi_city_form = $('#multi_city_form');
                var error = $('.alert-danger', multi_city_form);
                var success = $('.alert-success', multi_city_form);

                multi_city_form.validate({
                    errorElement: 'span', //default input error message container
                    errorClass: 'help-block help-block-error', // default input error message class
                    focusInvalid: false, // do not focus the last invalid input
                    ignore: "", // validate all fields including form hidden input
                    rules: {
                        from: {
                            required: true
                        },
                        to: {
                            required: true
                        },

                    },

                    messages: { // custom messages for radio buttons and checkboxes
                        oneway: {
                            required: "Please select a Trip type"
                        },
                    },

                    errorPlacement: function (error, element) { // render error placement for each input typeW
                        if (element.parents('.mt-radio-list').size() > 0 || element.parents('.mt-checkbox-list').size() > 0) {
                            if (element.parents('.mt-radio-list').size() > 0) {
                                error.appendTo(element.parents('.mt-radio-list')[0]);
                            }
                            if (element.parents('.mt-checkbox-list').size() > 0) {
                                error.appendTo(element.parents('.mt-checkbox-list')[0]);
                            }
                        } else if (element.parents('.mt-radio-inline').size() > 0 || element.parents('.mt-checkbox-inline').size() > 0) {
                            if (element.parents('.mt-radio-inline').size() > 0) {
                                error.appendTo(element.parents('.mt-radio-inline')[0]);
                            }
                            if (element.parents('.mt-checkbox-inline').size() > 0) {
                                error.appendTo(element.parents('.mt-checkbox-inline')[0]);
                            }
                        } else if (element.parent(".input-group").size() > 0) {
                            error.insertAfter(element.parent(".input-group"));
                        } else if (element.attr("data-error-container")) {
                            error.appendTo(element.attr("data-error-container"));
                        } else {
                            error.insertAfter(element); // for other inputs, just perform default behavior
                        }
                    },

                    invalidHandler: function (event, validator) { //display error alert on form submit
                        success.hide();
                        error.show();
                        //App.scrollTo(error3, -200);
                    },

                    highlight: function (element) { // hightlight error inputs
                        $(element)
                            .closest('.form-group').addClass('has-error'); // set error class to the control group
                    },

                    unhighlight: function (element) { // revert the change done by hightlight
                        $(element)
                            .closest('.form-group').removeClass('has-error'); // set error class to the control group
                    },

                    success: function (label) {
                        label
                            .closest('.form-group').removeClass('has-error'); // set success class to the control group
                    },

                    submitHandler: function (form) {
                        success.show();
                        error.hide();
                        form[0].submit(); // submit the form
                    }

                });

//            initialize datepicker
                $('.date-picker').datepicker({
                    autoclose: true,
                    startDate: 'd',
                });

                $('.date-picker .form-control').change(function() {
                    multi_city_form.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
                })
            }

            return {
                //main function to initiate the module
                init: function () {
                    roundTripFormValidation();
                    oneWayFormValidation();
                    multiCityValidation();
                }
            };

        }();

        jQuery(document).ready(function() {
            FormValidation.init();
        });

        // Add and remove discription
        var numberFieldMultiCity = 3;
        $('#add_another_destination_btn').on('click', function()
        {
            if(numberFieldMultiCity < 8)    //  maximum number of fields
            {
                $('#destinations').before('' +
                    '<div id="destinatin' + numberFieldMultiCity + '">' +
                        '<h5>Destination ' + (numberFieldMultiCity + 1 * 1) + '</h5>' +
                        '<div class="row">' +
                            '<div class="col-md-6">' +
                                '<div class="form-group control-group">' +
                                    '<label for="multy_city_select_from' + numberFieldMultiCity + '">From</label>' +
                                    '<select id="multy_city_select_from' + numberFieldMultiCity + '" name="multy_city_select_from[]" class="form-control airports selectize-form-search" required></select>' +
                                '</div>' +
                            '</div>' +
                            '<div class="col-md-6">' +
                                '<div class="form-group">' +
                                    '<label for="multy_city_select_to' + numberFieldMultiCity + '">To</label>' +
                                    '<select id="multy_city_select_to' + numberFieldMultiCity + '" name="multy_city_select_to[]" class="form-control airports selectize-form-search" required></select>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                        '<div class="row">' +
                            '<div class="col-md-6">' +
                                '<div class="form-group">' +
                                    '<label for="departure_date">Departure date</label>' +
                                    '<div class="input-group date date-picker" data-date-format="mm-dd-yyyy">' +
                                        '<input type="text" class="form-control" readonly name="departure_date[]">' +
                                        '<span class="input-group-btn">' +
                                            '<button class="btn default" type="button">' +
                                                '<i class="fa fa-calendar"></i>' +
                                            '</button>' +
                                        '</span>' +
                                    '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                    '<hr>' +
                    '');

                $('#multy_city_select_from'+numberFieldMultiCity+', #multy_city_select_to'+numberFieldMultiCity).selectize({
                    valueField: 'code',
                    labelField: 'city',
                    selectOnTab: 'true',
                    searchField: ['code', 'name', 'city'],
                    optgroupValueField: 'city_id',
                    optgroupField: 'city_id',
                    sortField: 'sort',
                    create: false,
                    render: {
                        option: function(item, escape) {
                            var airports = [];
                            for (var i = 0, n = item.length; i < n; i++) {
                                airports.push('<span>' + escape(item.city) +' (' + escape(item.code) +' ' + escape(item.name) +')</span>');
                            }

                            if(item.name == 'All Airports'){
                                var str = '<div>' +
                                    '<span style="font-weight:900;">' + escape(item.city)+', '+ item.country +' (' + escape(item.code) +' ' + escape(item.name) +')</span>' +
                                    '</div>';
                            }else{
                                var str = '<div>' +
                                    '<span>' + escape(item.city)+', '+ item.country +' (' + escape(item.code) +' ' + escape(item.name) +')</span>' +
                                    '</div>';
                            }

                            return str;
                        }
                    },
                    load: function(query, callback) {
                        if (!query.length) return callback();

                        var url = "{{route('main.search')}}";
                        var token = "{{ csrf_token() }}";

                        $.ajax({
                            url: url,
                            type: 'POST',
                            data: {q: query,_token:token,
                            },
                            error: function() {
                                callback();
                            },
                            success: function(res) {
                                console.log(res);
                                callback(res);
                            }
                        });
                    }
                });
                FormValidation.init();
                numberFieldMultiCity++;
            }
        });

        // remove
        $('#remove_destination_btn').on('click', function()
        {
            if(numberFieldMultiCity > 3)
            {
                $('#destinatin' + (numberFieldMultiCity - 1 * 1)).remove();
                numberFieldMultiCity--;
            }
        });
    </script>


@endsection

@section('stylesheets')
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker3.min.css')}}"/>
    <link rel="stylesheet" href="{{ asset('assets/selectize.js/dist/css/selectize.bootstrap3.css')}}"/>
    <style>
        .datepicker{
            margin-top:145px;
        }
        .trip-form .row{
            margin-top:10px;
            margin-bottom:10px;
        }
        .trip-form label{
            font-size: 0.9em;
        }

        /*Selectize*/
        .selectize-control.airports .selectize-dropdown [data-selectable] {
            border-bottom: 1px solid rgba(0,0,0,0.05);
            position: relative;
            -webkit-box-sizing: content-box;
            box-sizing: content-box;
            padding: 10px;
        }
        .selectize-control.airports .selectize-dropdown [data-selectable]:last-child {
            border-bottom: 0 none;
        }
        .selectize-control.airports .selectize-dropdown .by {
            font-size: 11px;
            opacity: 0.8;
        }
        .selectize-control.airports .selectize-dropdown .by::before {
            content: 'by ';
        }
        .selectize-control.airports .selectize-dropdown .name {
            font-weight: bold;
            margin-right: 5px;
        }
        .selectize-control.airports .selectize-dropdown .description {
            font-size: 12px;
            color: #a0a0a0;
        }
        .selectize-control.airports .selectize-dropdown .airports,
        .selectize-control.airports .selectize-dropdown .description,
        .selectize-control.airports .selectize-dropdown .title {
            display: block;
            white-space: nowrap;
            width: 100%;
            overflow: hidden;
            text-overflow: ellipsis;
        }
        .selectize-control.airports .selectize-dropdown .airports {
            font-size: 10px;
            color: #a0a0a0;
        }
        .selectize-control.airports .selectize-dropdown .airports span {
            color: #606060;
        }
        .selectize-control.airports .selectize-dropdown .meta {
            list-style: none;
            margin: 0;
            padding: 0;
            font-size: 10px;
        }
        .selectize-control.airports .selectize-dropdown .meta li {
            margin: 0;
            padding: 0;
            display: inline;
            margin-right: 10px;
        }
        .selectize-control.airports .selectize-dropdown .meta li span {
            font-weight: bold;
        }
        .selectize-control.airports::before {
            -moz-transition: opacity 0.2s;
            -webkit-transition: opacity 0.2s;
            transition: opacity 0.2s;
            content: ' ';
            z-index: 2;
            position: absolute;
            display: block;
            top: 12px;
            right: 34px;
            width: 16px;
            height: 16px;
            background: url(/img/spinner.gif);
            background-size: 16px 16px;
            opacity: 0;
        }
        .selectize-control.airports.loading::before {
            opacity: 0.4;
        }
    </style>
@endsection