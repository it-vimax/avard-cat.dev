<!doctype html>
<html lang="en">

@include('user.partials._head')

<body>

<div id="parallax">
</div>

@include('user.partials._header')

@yield('content')

@include('user.partials._footer')

@include('user.partials._javascript')

</body>
</html>