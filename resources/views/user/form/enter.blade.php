@extends('user.main')

@section('title', 'Step 3')
@section('description', 'Service for spending points.')

@section('content')
    <section id="form-section" class="parallax section" style="background-image: url('/img/1.jpg');">
        <div class="wrapsection">
            <div class="parallax-overlay" style="background-color: #0023f2;opacity:0.9;"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Title -->
                        <div class="maintitle">
                            <h3 class="section-title">Enter</h3>
                            <p class="lead" style="margin-bottom:20px;">
                                Complete Registration or Sing In.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-12 col-md-offset-2">
                        <div class="service-box wow zoomIn" data-wow-duration="1.5s" data-wow-delay="0.1s">


                            <h4>Enter your details</h4>
                            {!! Form::open(['route' => 'registration', 'method'=>'POST']) !!}

                            <div class="row">
                                <div class="cool-md-6">
                                    <h5>Member information</h5>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group {{ $errors->has('first_name') ? ' has-error' : '' }}">
                                        <label for="usr">First name:</label>

                                        @if(null !== Session::has("AwardWallet.firstName"))
                                            <input type="text" class="form-control " id="first_name" name="first_name" value="{{ Session::get("AwardWallet.firstName") }}">
                                        @else
                                            <input type="text" class="form-control " id="first_name" name="first_name" value="{{ old('first_name') }}">
                                        @endif

                                        @if($errors->has('first_name'))
                                            <strong class="help-block">{{ $errors->first('first_name') }}</strong>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group {{ $errors->has('last_name') ? ' has-error' : '' }}">
                                        <label for="usr">Last name:</label>

                                        @if(null !== Session::has("AwardWallet.lastName"))
                                            <input type="text" class="form-control " id="last_name" name="last_name" value="{{ Session::get("AwardWallet.lastName") }}">
                                        @else
                                            <input type="text" class="form-control" id="last_name" name="last_name" value="{{ old('last_name') }}">
                                        @endif

                                        @if($errors->has('last_name'))
                                            <strong class="help-block">{{ $errors->first('last_name') }}</strong>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="cool-md-6">
                                    <h5>Contact information</h5>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group {{ $errors->has('street_address_line_1') ? ' has-error' : '' }}">
                                        <label for="street_address_line_1">Street address line 1</label>
                                        <input type="text" class="form-control" id="street_address_line_1" name="street_address_line_1" value="{{ old('street_address_line_1') }}">
                                        @if($errors->has('street_address_line_1'))
                                            <strong class="help-block">{{ $errors->first('street_address_line_1') }}</strong>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group {{ $errors->has('street_address_line_2') ? ' has-error' : '' }}">
                                        <label for="street_address_line_2">Street address line 2</label>
                                        <input type="text" class="form-control" id="street_address_line_2" name="street_address_line_2" value="{{ old('street_address_line_2') }}">
                                        @if($errors->has('street_address_line_2'))
                                            <strong class="help-block">{{ $errors->first('street_address_line_2') }}</strong>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group {{ $errors->has('city_town_department') ? ' has-error' : '' }}">
                                        <label for="city_town_department">City/town/department</label>
                                        <input type="text" class="form-control" id="city_town_department" name="city_town_department" value="{{ old('city_town_department') }}">
                                        @if($errors->has('city_town_department'))
                                            <strong class="help-block">{{ $errors->first('city_town_department') }}</strong>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group {{ $errors->has('state_provice_region_country') ? ' has-error' : '' }}">
                                        <label for="state_provice_region_country">State/provice/region/country</label>
                                        <input type="text" class="form-control" id="state_provice_region_country" name="state_provice_region_country" value="{{ old('state_provice_region_country') }}">
                                        @if($errors->has('state_provice_region_country'))
                                            <strong class="help-block">{{ $errors->first('state_provice_region_country') }}</strong>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group {{ $errors->has('zip_personal_code') ? ' has-error' : '' }}">
                                        <label for="zip_personal_code">ZIP/Postal code</label>
                                        <input type="text" class="form-control" id="zip_personal_code" name="zip_personal_code" value="{{ old('zip_personal_code') }}" value="{{ old('zip_personal_code') }}">
                                        @if($errors->has('zip_personal_code'))
                                            <strong class="help-block">{{ $errors->first('zip_personal_code') }}</strong>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <span>Country</span>
                                    <select class="form-control" name="country" id="country">
                                        <option selected="selected" value="US">United States</option>
                                        <option value="CA">Canada</option>
                                        <option value="GB">United Kingdom</option>
                                        <option value="AF">Afghanistan</option>
                                        <option value="AL">Albania</option>
                                        <option value="DZ">Algeria</option>
                                        <option value="AS">American Samoa</option>
                                        <option value="AD">Andorra</option>
                                        <option value="AO">Angola</option>
                                        <option value="AI">Anguilia</option>
                                        <option value="AQ">Antarctica</option>
                                        <option value="AG">Antigua and Barbuda</option>
                                        <option value="AR">Argentina</option>
                                        <option value="AM">Armenia</option>
                                        <option value="AW">Aruba</option>
                                        <option value="AU">Australia</option>
                                        <option value="AT">Austria</option>
                                        <option value="AZ">Azerbaijan</option>
                                        <option value="BS">Bahamas</option>
                                        <option value="BH">Bahrain</option>
                                        <option value="BD">Bangladesh</option>
                                        <option value="BB">Barbados</option>
                                        <option value="BY">Belarus</option>
                                        <option value="BE">Belgium</option>
                                        <option value="BZ">Belize</option>
                                        <option value="BJ">Benin</option>
                                        <option value="BM">Bermuda</option>
                                        <option value="BT">Bhutan</option>
                                        <option value="BO">Bolivia</option>
                                        <option value="BQ">Bonaire, Sint Eustatius and Saba</option>
                                        <option value="BA">Bosnia and Herzegovina</option>
                                        <option value="BW">Botswana</option>
                                        <option value="BR">Brazil</option>
                                        <option value="IO">British Indian Ocean Territory</option>
                                        <option value="VG">British Virgin Islands</option>
                                        <option value="BN">Brunei</option>
                                        <option value="BG">Bulgaria</option>
                                        <option value="BF">Burkina Faso</option>
                                        <option value="BI">Burundi</option>
                                        <option value="KH">Cambodia</option>
                                        <option value="CM">Cameroon</option>
                                        <option value="CV">Cape Verde</option>
                                        <option value="KY">Cayman Islands</option>
                                        <option value="CF">Central African Republic</option>
                                        <option value="TD">Chad</option>
                                        <option value="CL">Chile</option>
                                        <option value="CN">China, People&#39;s Republic of</option>
                                        <option value="CX">Christmas Island</option>
                                        <option value="CC">Cocos (Keeling) Islands</option>
                                        <option value="CO">Colombia</option>
                                        <option value="KM">Comoros</option>
                                        <option value="CG">Congo</option>
                                        <option value="CD">Congo, Democratic Republic of the (Zaire)</option>
                                        <option value="CK">Cook Islands</option>
                                        <option value="CR">Costa Rica</option>
                                        <option value="CI">Cote d&#39;ivoire</option>
                                        <option value="HR">Croatia</option>
                                        <option value="CW">Curaçao</option>
                                        <option value="CY">Cyprus</option>
                                        <option value="CZ">Czech Republic</option>
                                        <option value="DK">Denmark</option>
                                        <option value="DG">Diego Garcia</option>
                                        <option value="DJ">Djibouti</option>
                                        <option value="DM">Dominica</option>
                                        <option value="DO">Dominican Republic</option>
                                        <option value="TP">East Timor</option>
                                        <option value="EC">Ecuador</option>
                                        <option value="EG">Egypt</option>
                                        <option value="SV">El Salvador</option>
                                        <option value="GQ">Equatorial Guinea</option>
                                        <option value="ER">Eritrea</option>
                                        <option value="EE">Estonia</option>
                                        <option value="ET">Ethiopia</option>
                                        <option value="FK">Falkland Islands</option>
                                        <option value="FO">Faroe Islands</option>
                                        <option value="FJ">Fiji</option>
                                        <option value="FI">Finland</option>
                                        <option value="FR">France</option>
                                        <option value="GF">French Guiana</option>
                                        <option value="PF">French Polynesia</option>
                                        <option value="GA">Gabon</option>
                                        <option value="GM">Gambia</option>
                                        <option value="GE">Georgia</option>
                                        <option value="DE">Germany</option>
                                        <option value="GH">Ghana</option>
                                        <option value="GI">Gibraltar</option>
                                        <option value="GR">Greece</option>
                                        <option value="GL">Greenland</option>
                                        <option value="GD">Grenada</option>
                                        <option value="GP">Guadeloupe</option>
                                        <option value="GU">Guam</option>
                                        <option value="GT">Guatemala</option>
                                        <option value="GN">Guinea</option>
                                        <option value="GW">Guinea-Bissau</option>
                                        <option value="GY">Guyana</option>
                                        <option value="HT">Haiti</option>
                                        <option value="HN">Honduras</option>
                                        <option value="HK">Hong Kong</option>
                                        <option value="HU">Hungary</option>
                                        <option value="IS">Iceland</option>
                                        <option value="IN">India</option>
                                        <option value="ID">Indonesia</option>
                                        <option value="IQ">Iraq</option>
                                        <option value="IE">Ireland</option>
                                        <option value="IL">Israel</option>
                                        <option value="IT">Italy</option>
                                        <option value="JM">Jamaica</option>
                                        <option value="JP">Japan</option>
                                        <option value="JO">Jordan</option>
                                        <option value="KZ">Kazakhstan</option>
                                        <option value="KE">Kenya</option>
                                        <option value="KI">Kiribati</option>
                                        <option value="KR">Korea, South</option>
                                        <option value="KW">Kuwait</option>
                                        <option value="KG">Kyrgyzstan</option>
                                        <option value="LA">Laos</option>
                                        <option value="LV">Latvia</option>
                                        <option value="LB">Lebanon</option>
                                        <option value="LS">Lesotho</option>
                                        <option value="LR">Liberia</option>
                                        <option value="LY">Libya</option>
                                        <option value="LI">Liechtenstein</option>
                                        <option value="LT">Lithuania</option>
                                        <option value="LU">Luxembourg</option>
                                        <option value="MO">Macau</option>
                                        <option value="MK">Macedonia (FYROM)</option>
                                        <option value="MG">Madagascar</option>
                                        <option value="MW">Malawi</option>
                                        <option value="MY">Malaysia</option>
                                        <option value="MV">Maldives</option>
                                        <option value="ML">Mali</option>
                                        <option value="MT">Malta</option>
                                        <option value="MH">Marshall Islands</option>
                                        <option value="MQ">Martinique</option>
                                        <option value="MR">Mauritania</option>
                                        <option value="MU">Mauritius</option>
                                        <option value="YT">Mayotte</option>
                                        <option value="MX">Mexico</option>
                                        <option value="FM">Micronesia</option>
                                        <option value="MD">Moldova</option>
                                        <option value="MC">Monaco</option>
                                        <option value="MN">Mongolia</option>
                                        <option value="ME">Montenegro</option>
                                        <option value="MS">Montserrat</option>
                                        <option value="MA">Morocco</option>
                                        <option value="MZ">Mozambique</option>
                                        <option value="MM">Myanmar (Burma)</option>
                                        <option value="NA">Namibia</option>
                                        <option value="NR">Nauru</option>
                                        <option value="NP">Nepal</option>
                                        <option value="NL">Netherlands</option>
                                        <option value="NC">New Caledonia</option>
                                        <option value="NZ">New Zealand</option>
                                        <option value="NI">Nicaragua</option>
                                        <option value="NE">Niger</option>
                                        <option value="NG">Nigeria</option>
                                        <option value="NU">Niue</option>
                                        <option value="NF">Norfolk Island</option>
                                        <option value="MP">Northern Mariana Islands</option>
                                        <option value="NO">Norway</option>
                                        <option value="OM">Oman</option>
                                        <option value="PK">Pakistan</option>
                                        <option value="PW">Palau</option>
                                        <option value="PS">Palestinian Territories</option>
                                        <option value="PA">Panama</option>
                                        <option value="PG">Papua New Guinea</option>
                                        <option value="PY">Paraguay</option>
                                        <option value="PE">Peru</option>
                                        <option value="PH">Philippines</option>
                                        <option value="PN">Pitcairn</option>
                                        <option value="PL">Poland</option>
                                        <option value="PT">Portugal</option>
                                        <option value="PR">Puerto Rico</option>
                                        <option value="QA">Qatar</option>
                                        <option value="RE">Reunion</option>
                                        <option value="RO">Romania</option>
                                        <option value="RU">Russia</option>
                                        <option value="RW">Rwanda</option>
                                        <option value="GS">South Georgia and the South Sandwich Islands</option>
                                        <option value="BL">Saint Barthélemy</option>
                                        <option value="KN">Saint Kitts and Nevis</option>
                                        <option value="LC">Saint Lucia</option>
                                        <option value="MF">Saint Martin (France)</option>
                                        <option value="VC">Saint Vincent and the Grenadines</option>
                                        <option value="WS">Samoa</option>
                                        <option value="SM">San Marino</option>
                                        <option value="ST">Sao Tome and Principe</option>
                                        <option value="SA">Saudi Arabia</option>
                                        <option value="SN">Senegal</option>
                                        <option value="RS">Serbia</option>
                                        <option value="SC">Seychelles</option>
                                        <option value="SL">Sierra Leone</option>
                                        <option value="SG">Singapore</option>
                                        <option value="SX">Sint Maarten (Netherlands)</option>
                                        <option value="SK">Slovakia</option>
                                        <option value="SI">Slovenia</option>
                                        <option value="SB">Solomon Islands</option>
                                        <option value="SO">Somalia</option>
                                        <option value="ZA">South Africa</option>
                                        <option value="SS">South Sudan</option>
                                        <option value="ES">Spain &amp; Canary Islands</option>
                                        <option value="LK">Sri Lanka</option>
                                        <option value="SH">Saint Helena</option>
                                        <option value="PM">Saint Pierre and Miquelon</option>
                                        <option value="SR">Suriname</option>
                                        <option value="SJ">Svalbard and Jan Mayen Islands</option>
                                        <option value="SZ">Swaziland</option>
                                        <option value="SE">Sweden</option>
                                        <option value="CH">Switzerland</option>
                                        <option value="TW">Taiwan</option>
                                        <option value="TJ">Tajikistan</option>
                                        <option value="TZ">Tanzania</option>
                                        <option value="TH">Thailand</option>
                                        <option value="TG">Togo</option>
                                        <option value="TK">Tokelau</option>
                                        <option value="TO">Tonga</option>
                                        <option value="TT">Trinidad and Tobago</option>
                                        <option value="TN">Tunisia</option>
                                        <option value="TR">Turkey</option>
                                        <option value="TM">Turkmenistan</option>
                                        <option value="TC">Turks and Caicos Islands</option>
                                        <option value="TV">Tuvalu</option>
                                        <option value="UG">Uganda</option>
                                        <option value="UA">Ukraine</option>
                                        <option value="AE">United Arab Emirates</option>
                                        <option value="UM">United States Minor Outlying Islands</option>
                                        <option value="UY">Uruguay</option>
                                        <option value="UZ">Uzbekistan</option>
                                        <option value="VU">Vanuatu</option>
                                        <option value="VA">Vatican City State (Holy See)</option>
                                        <option value="VE">Venezuela</option>
                                        <option value="VN">Vietnam</option>
                                        <option value="VI">U.S. Virgin Islands</option>
                                        <option value="WF">Wallis and Futuna</option>
                                        <option value="YE">Yemen</option>
                                        <option value="ZM">Zambia</option>
                                        <option value="ZW">Zimbabwe</option>
                                    </select>

                                </div>
                            </div>

                            <div class="row">
                                <div class="cool-md-6">
                                    <h5>Phone number</h5>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group {{ $errors->has('mobile_phone_number') ? ' has-error' : '' }}">
                                        <label for="mobile_phone_number">Mobile phone number</label>
                                        <input type="text" class="form-control" id="mobile_phone_number" name="mobile_phone_number" value="{{ old('mobile_phone_number') }}">
                                        @if($errors->has('mobile_phone_number'))
                                            <strong class="help-block">{{ $errors->first('mobile_phone_number') }}</strong>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group {{ $errors->has('business_phone_number') ? ' has-error' : '' }}">
                                        <label for="business_phone_number">Business phone number</label>
                                        <input type="text" class="form-control" id="business_phone_number" name="business_phone_number" value="{{ old('business_phone_number') }}">
                                        @if($errors->has('business_phone_number'))
                                            <strong class="help-block">{{ $errors->first('business_phone_number') }}</strong>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="cool-md-6">
                                    <h5>Email information</h5>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group {{ $errors->has('email_address') ? ' has-error' : '' }}">
                                        <label for="email_address">Email address</label>


                                        @if(null !== Session::has("AwardWallet.email"))
                                            <input type="email" class="form-control " id="email_address" name="email_address" value="{{ Session::get("AwardWallet.email") }}">
                                        @else
                                            <input type="email" class="form-control" id="email_address" name="email_address" value="{{ old('email_address') }}">
                                        @endif

                                        @if($errors->has('email_address'))
                                            <strong class="help-block">{{ $errors->first('email_address') }}</strong>
                                        @endif
                                    </div>
                                </div>

                                {{--<div class="col-md-6">--}}
                                    {{--<div class="form-group {{ $errors->has('verify_email_address') ? ' has-error' : '' }}">--}}
                                        {{--<label for="verify_email_address">Verify Email Address</label>--}}

                                        {{--@if(null !== Session::has("AwardWallet.email"))--}}
                                            {{--<input type="email" class="form-control " id="verify_email_address" name="verify_email_address" value="{{ Session::get("AwardWallet.email") }}">--}}
                                        {{--@else--}}
                                            {{--<input type="email" class="form-control" id="verify_email_address" name="verify_email_address" value="{{ old('verify_email_address') }}">--}}
                                        {{--@endif--}}

                                        {{--@if($errors->has('verify_email_address'))--}}
                                            {{--<strong class="help-block">{{ $errors->first('verify_email_address') }}</strong>--}}
                                        {{--@endif--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                            </div>

                            <div class="row">
                                <div class="cool-md-6">
                                    <h5>Security Information</h5>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label for="password">Enter Password</label>
                                        <input type="password" class="form-control" id="password" name="password">
                                        @if($errors->has('password'))
                                            <strong class="help-block">{{ $errors->first('password') }}</strong>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group {{ $errors->has('verify_password') ? ' has-error' : '' }}">
                                        <label for="verify_password">Confirm Password</label>
                                        <input type="password" class="form-control" id="verify_password" name="verify_password">
                                        @if($errors->has('verify_password'))
                                            <strong class="help-block">{{ $errors->first('verify_password') }}</strong>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-block btn-primary">Registration</button>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}

                            <div class="row">
                                <div class="col-md-12">
                                <h5>OR</h5>
                                </div>
                            </div>

                            {!! Form::open(['route' => 'stepFour', 'method'=>'POST']) !!}
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-block btn-primary">Sing in</button>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('javascript')

    <script src="{{ asset('assets/select2-4.0.3/dist/js/select2.min.js')}}"></script>

    <script type="text/javascript">

        //Select2
        //        $(document).ready(function() {
        //            $(".select_bonus_programs").select2();
        //        });

        //Add additional bonus programs
        var i = 0;

        $('.add_additional_bonus_programs').on('click', function(event){
            event.preventDefault();
            if(i < 9){
                $('#main_bonus_programs').clone().appendTo('#additional_bonus_programs');
                i++;
            }
        });
    </script>


@endsection

@section('stylesheets')
    <link rel="stylesheet" href="{{ asset('assets/select2-4.0.3/dist/css/select2.min.css')}}"/>
    <style>
        .select2-container--default .select2-selection--single{
            font-weight: 300;
            border-color: #ecf0f1;
            background-color: #ecf0f1;
            border-radius: 0px;
            -webkit-border-radius: 0px;
            -moz-border-radius: 0px;
            box-shadow: none;
            -webkit-box-shadow: none;
            -moz-box-shadow: none;
            -moz-transition: all .2s;
            -o-transition: all .2s;
            -webkit-transition: all .2s;
            transition: all .2s;
            display: block;
            width: 100%;
            height: 34px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
        }
        .select2-selection__rendered{
            margin-top:-4px;
        }
        .select2-selection__arrow{
            margin-top:4px;
        }
        .wrapsection{
            padding-top: 150px;
        }
    </style>
@endsection