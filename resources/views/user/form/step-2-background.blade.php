<div class="price_for_airlines_container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="well" style="text-align: center">
                <strong style="font-size:1.5em;" id="price_for_airlines">${{$output['price_for_airlines']}}</strong>
                <div>
                    <small>For {{session('adults')}} {{(session('adults') > 1)?'persons':'person'}} in
                        @if(session('trip_type') == 'Round-trip')
                            both ways
                        @elseif(session('trip_type') == 'One-way')
                            one way
                        @endif
                        in {{session('class')}} class.</small>
                </div>
                <div>
                    <a id="more-info-flight"><small>More info about flights</small></a>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="fares-container">
@foreach($airlines as $airline => $fares)
    @foreach($fares as $fare)
        <div class="well fare" style="text-align: left;">
            <div class="row">
                <div class="col-md-12">
                    <i class="fa fa-plane"></i> {{$airline}}
                </div>
                {{--<div class="col-md-4" style="text-align: right;">--}}
                    {{--<i class="fa fa-usd"></i> {{$fare['price']}}--}}
                {{--</div>--}}
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div><i class="fa fa-map-marker"></i> From: {{$fare['from_city']}}, {{$fare['from']}}</div>
                    <div>
                        <i class="fa fa-clock-o"></i>
                        @foreach($fare['dep_time'] as $dep_time)
                            @if($dep_time == 'N')
                                <span>Night</span>{!! ($loop->iteration >= 1 && $loop->iteration < count($fare['dep_time']))?'<span>,</span>':'' !!}
                            @elseif($dep_time == 'M')
                                <span>Morning</span>{!! ($loop->iteration >= 1 && $loop->iteration < count($fare['dep_time']))?'<span>,</span>':'' !!}
                            @elseif($dep_time == 'D')
                                <span>Day</span>{!! ($loop->iteration >= 1 && $loop->iteration < count($fare['dep_time']))?'<span>,</span>':'' !!}
                            @elseif($dep_time == 'E')
                                <span>Evening</span>{!! ($loop->iteration >= 1 && $loop->iteration < count($fare['dep_time']))?'<span>,</span>':'' !!}
                            @endif
                        @endforeach
                    </div>
                </div>
                <div class="col-md-4">
                    <div>Time of flight: {{$fare['time_of_flight']}}</div>
                    <div>Class: {{$fare['class']}}</div>
                </div>
                <div class="col-md-4">
                    <div><i class="fa fa-map-marker"></i> To: {{$fare['to_city']}}, {{$fare['to']}}</div>
                    <div>
                        <i class="fa fa-clock-o"></i>
                        @foreach($fare['arr_time'] as $arr_time)
                            @if($arr_time == 'N')
                                <span>Night</span>{!! ($loop->iteration >= 1 && $loop->iteration < count($fare['arr_time']))?'<span>,</span>':'' !!}
                            @elseif($arr_time == 'M')
                                <span>Morning</span>{!! ($loop->iteration >= 1 && $loop->iteration < count($fare['arr_time']))?'<span>,</span>':'' !!}
                            @elseif($arr_time == 'D')
                                <span>Day</span>{!! ($loop->iteration >= 1 && $loop->iteration < count($fare['arr_time']))?'<span>,</span>':'' !!}
                            @elseif($arr_time == 'E')
                                <span>Evening</span>{!! ($loop->iteration >= 1 && $loop->iteration < count($fare['arr_time']))?'<span>,</span>':'' !!}
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endforeach
</div>

<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <a id="anywayanyday_link" href="{{$output['url']}}" class="btn btn-block btn-primary" target="_blank">Buy with cash</a>
    </div>
</div>

{{--<div class="row" style="margin-bottom: 20px;">--}}
    {{--<div class="col-md-4 col-md-offset-4">--}}
        {{--<button id="show_more_arln" class="btn btn-block btn-primary"><i class="fa fa-plus-square" aria-hidden="true"></i> Show More</button>--}}
    {{--</div>--}}
{{--</div>--}}


