@extends('user.main')

@section('title', 'Step 4')
@section('description', 'Service for spending points.')

@section('content')
    <section id="form-section" class="parallax section" style="background-image: url('/img/1.jpg');">
        <div class="wrapsection">
            <div class="parallax-overlay" style="background-color: #0023f2;opacity:0.9;"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Title -->
                        <div class="maintitle">
                            <h3 class="section-title">Step 4. Success Some text here.</h3>
                            <p class="lead" style="margin-bottom:20px;">
                                Do we need some additional text
                            </p>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-12 col-md-offset-2">
                        <div class="service-box wow zoomIn" data-wow-duration="1.5s" data-wow-delay="0.1s">
                            <h4>Please Pay $100. Pay system here.</h4>
                            {!! Form::open(['route' => 'enter', 'method'=>'POST']) !!}
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-block btn-primary">Pay</button>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}


                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection

@section('javascript')



@endsection

@section('stylesheets')
    <style>
        .wrapsection{
            padding-top: 150px;
        }
    </style>
@endsection