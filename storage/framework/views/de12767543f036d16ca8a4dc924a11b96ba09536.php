<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li class="nav-item start <?php echo e(Request::is('admin') ? 'active open' : ''); ?>">
                <a href="<?php echo e(asset('/admin')); ?>" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                    <span class="arrow"></span>
                </a>
            </li>

            <li class="nav-item  <?php echo e(Request::is('admin/trips') ? 'active open' : ''); ?>">
                <a href="<?php echo e(asset('/admin/trips')); ?>" class="nav-link nav-toggle">
                    <i class="icon-social-dribbble"></i>
                    <span class="title">Trips</span>
                    <span class="arrow"></span>
                </a>
            </li>

        </ul>
    </div>
</div>