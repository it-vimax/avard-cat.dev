<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<?php echo $__env->make('admin.partials._head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid">

    <?php echo $__env->make('admin.partials._header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <div class="page-container">

        <?php echo $__env->make('admin.partials._sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <div class="page-content-wrapper">
            <div class="page-content">
                <?php echo $__env->yieldContent('content'); ?>
            </div>
        </div>

    </div>

    <?php echo $__env->make('admin.partials._footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('admin.partials._javascript', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->yieldContent('javascript'); ?>
    <?php echo $__env->make('admin.partials._modalNotification', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

</body>

</html>