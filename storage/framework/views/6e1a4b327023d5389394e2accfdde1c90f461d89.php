<script src="<?php echo e(asset('js/jquery.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/waypoints.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/jquery.scrollTo.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/jquery.localScroll.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/jquery.magnific-popup.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/validate.js')); ?>"></script>
<script src="<?php echo e(asset('js/common.js')); ?>"></script>
<?php echo $__env->yieldContent('javascript'); ?>