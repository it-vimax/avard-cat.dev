<?php $__env->startSection('title', 'Admin Dashboard'); ?>

<?php $__env->startSection('content'); ?>

<div class="row">

    <div class="col-md-8">
        <div class="portlet box blue-hoki">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa icon-social-dribbble"></i>Direction Info [ Trip ID - <?php echo e($trip->id); ?> ] </div>
            </div>
            <div class="portlet-body">
                <div class="table-scrollable">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th> # </th>
                            <th> Trip Type </th>
                            <th> From Code</th>
                            <th> From City </th>
                            <th> From Country </th>
                            <th> To Code</th>
                            <th> To City </th>
                            <th> To Country </th>

                            <?php if($trip->flexible_checked): ?>
                                <th> Flexible Date </th>
                                <th> Flexible Period </th>
                            <?php else: ?>
                                <th> Departure Date </th>
                                <?php if(!empty($trip->directions[0]->return_date)): ?>
                                    <th> Return Date </th>
                                <?php endif; ?>
                            <?php endif; ?>

                            <th> Adults </th>
                            <th> Class </th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php $__currentLoopData = $trip->directions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $direction): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td> <?php echo e($loop->iteration); ?> </td>
                                <td> <?php echo e($direction->trip_type); ?> </td>
                                <td> <?php echo e($direction->from); ?> </td>
                                <td> <?php echo e($locality_directions['from'][$loop->iteration - 1]['city']); ?> </td>
                                <td> <?php echo e($locality_directions['from'][$loop->iteration - 1]['country']); ?> </td>
                                <td> <?php echo e($direction->to); ?> </td>
                                <td> <?php echo e($locality_directions['to'][$loop->iteration - 1]['city']); ?> </td>
                                <td> <?php echo e($locality_directions['to'][$loop->iteration - 1]['country']); ?> </td>

                                <?php if($trip->flexible_checked): ?>
                                    <td> <?php echo e($trip->flexible_date); ?> </td>
                                    <td> <?php echo e($trip->flexible_period); ?> </td>
                                <?php else: ?>
                                    <td> <?php echo e(\Carbon\Carbon::createFromFormat('Y-m-d', $direction->departure_date)->format('m-d-Y')); ?> </td>
                                    <?php if(!empty($direction->return_date)): ?>
                                        <td> <?php echo e(\Carbon\Carbon::createFromFormat('Y-m-d', $direction->return_date)->format('m-d-Y')); ?> </td>
                                    
                                        
                                    <?php endif; ?>
                                <?php endif; ?>

                                <td> <?php echo e($direction->adults); ?> </td>
                                <td> <?php echo e($direction->class); ?> </td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="portlet box blue-hoki">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa icon-user"></i>Contact Info [ Client ID - <?php echo e($trip->client->id); ?>]</div>
            </div>
            <div class="portlet-body">
                <div class="table-scrollable">
                    <table class="table table-hover">
                        <tr>
                            <th> First Name </th>
                            <td> <?php echo e($trip->client->first_name); ?> </td>
                        </tr>
                        <tr>
                            <th> Last Name </th>
                            <td> <?php echo e($trip->client->last_name); ?> </td>
                        </tr>
                        <tr>
                            <th> Email </th>
                            <td> <?php echo e($trip->client->user->email); ?> </td>
                        </tr>
                        <tr>
                            <th> Mobile phone </th>
                            <td> <?php echo e($trip->client->mobile_phone_number); ?> </td>
                        </tr>
                        <tr>
                            <th> Bussines phone </th>
                            <td> <?php echo e($trip->client->business_phone_number); ?> </td>
                        </tr>
                        <tr>
                            <th> Country </th>
                            <td> <?php echo e($trip->client->country); ?> </td>
                        </tr>
                        <tr>
                            <th> City </th>
                            <td> <?php echo e($trip->client->city_town_department); ?> </td>
                        </tr>
                        <tr>
                            <th> ZIP </th>
                            <td> <?php echo e($trip->client->zip_personal_code); ?> </td>
                        </tr>
                        <tr>
                            <th> Street Address 1 </th>
                            <td> <?php echo e($trip->client->street_address_line_1); ?> </td>
                        </tr>
                        <tr>
                            <th> Street Address 2 </th>
                            <td> <?php echo e($trip->client->street_address_line_2); ?> </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('stylesheets'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>