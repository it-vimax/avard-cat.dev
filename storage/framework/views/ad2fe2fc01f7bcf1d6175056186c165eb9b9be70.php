<div class="price_for_airlines_container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="well" style="text-align: center">
                <strong style="font-size:1.5em;" id="price_for_airlines">$<?php echo e($output['price_for_airlines']); ?></strong>
                <div>
                    <small>For <?php echo e(session('adults')); ?> <?php echo e((session('adults') > 1)?'persons':'person'); ?> in
                        <?php if(session('trip_type') == 'Round-trip'): ?>
                            both ways
                        <?php elseif(session('trip_type') == 'One-way'): ?>
                            one way
                        <?php endif; ?>
                        in <?php echo e(session('class')); ?> class.</small>
                </div>
                <div>
                    <a id="more-info-flight"><small>More info about flights</small></a>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="fares-container">
<?php $__currentLoopData = $airlines; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $airline => $fares): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <?php $__currentLoopData = $fares; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fare): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="well fare" style="text-align: left;">
            <div class="row">
                <div class="col-md-12">
                    <i class="fa fa-plane"></i> <?php echo e($airline); ?>

                </div>
                
                    
                
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div><i class="fa fa-map-marker"></i> From: <?php echo e($fare['from_city']); ?>, <?php echo e($fare['from']); ?></div>
                    <div>
                        <i class="fa fa-clock-o"></i>
                        <?php $__currentLoopData = $fare['dep_time']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dep_time): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($dep_time == 'N'): ?>
                                <span>Night</span><?php echo ($loop->iteration >= 1 && $loop->iteration < count($fare['dep_time']))?'<span>,</span>':''; ?>

                            <?php elseif($dep_time == 'M'): ?>
                                <span>Morning</span><?php echo ($loop->iteration >= 1 && $loop->iteration < count($fare['dep_time']))?'<span>,</span>':''; ?>

                            <?php elseif($dep_time == 'D'): ?>
                                <span>Day</span><?php echo ($loop->iteration >= 1 && $loop->iteration < count($fare['dep_time']))?'<span>,</span>':''; ?>

                            <?php elseif($dep_time == 'E'): ?>
                                <span>Evening</span><?php echo ($loop->iteration >= 1 && $loop->iteration < count($fare['dep_time']))?'<span>,</span>':''; ?>

                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div>Time of flight: <?php echo e($fare['time_of_flight']); ?></div>
                    <div>Class: <?php echo e($fare['class']); ?></div>
                </div>
                <div class="col-md-4">
                    <div><i class="fa fa-map-marker"></i> To: <?php echo e($fare['to_city']); ?>, <?php echo e($fare['to']); ?></div>
                    <div>
                        <i class="fa fa-clock-o"></i>
                        <?php $__currentLoopData = $fare['arr_time']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $arr_time): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($arr_time == 'N'): ?>
                                <span>Night</span><?php echo ($loop->iteration >= 1 && $loop->iteration < count($fare['arr_time']))?'<span>,</span>':''; ?>

                            <?php elseif($arr_time == 'M'): ?>
                                <span>Morning</span><?php echo ($loop->iteration >= 1 && $loop->iteration < count($fare['arr_time']))?'<span>,</span>':''; ?>

                            <?php elseif($arr_time == 'D'): ?>
                                <span>Day</span><?php echo ($loop->iteration >= 1 && $loop->iteration < count($fare['arr_time']))?'<span>,</span>':''; ?>

                            <?php elseif($arr_time == 'E'): ?>
                                <span>Evening</span><?php echo ($loop->iteration >= 1 && $loop->iteration < count($fare['arr_time']))?'<span>,</span>':''; ?>

                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>

<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <a id="anywayanyday_link" href="<?php echo e($output['url']); ?>" class="btn btn-block btn-primary" target="_blank">Buy with cash</a>
    </div>
</div>


    
        
    



