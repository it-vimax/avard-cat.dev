<?php $__env->startSection('title', 'Step 3'); ?>
<?php $__env->startSection('description', 'Service for spending points.'); ?>

<?php $__env->startSection('content'); ?>
    <section id="form-section" class="parallax section" style="background-image: url('/img/1.jpg');">
        <div class="wrapsection">
            <div class="parallax-overlay" style="background-color: #0023f2;opacity:0.9;"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Title -->
                        <div class="maintitle">
                            <h3 class="section-title">Step 3. How much bonus points do you have?</h3>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-12 col-md-offset-2">
                        <div class="service-box wow zoomIn" data-wow-duration="1.5s" data-wow-delay="0.1s">

                            <div class="form-group">
                                <a class="btn btn-block" href="<?php echo e(asset('awardwallet/auth')); ?>">
                                    <img src="<?php echo e(asset('/img/awardwallet_logo.png')); ?>" alt="AwardWallet">
                                    <p id="awardwallet_helper_info_icon">Connect To AwardWallet*</p>
                                </a>
                            </div>

                            <h4>Or do it by your self</h4>
                            <?php echo Form::open(['route' => 'stepFour', 'method'=>'POST']); ?>

                            <div class="row">
                                <div class="col-md-10 col-md-offset-1">

                                    <?php if(!isset(Session::all()['AwardWallet']['account'])): ?>
                                        <div class="row" id="main_bonus_programs" style="margin-top:20px; margin-bottom:20px;">
                                            <div class="col-md-6">
                                                <div class="form-group control-group">
                                                    <select class="form-control selectize-form-search" name="bonus_programs[]" ></select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="number" class="form-control" name="number_of_points[]" placeholder="Number of Points" required />
                                            </div>
                                        </div>
                                    <?php endif; ?>

                                    <div id="additional_bonus_programs">

                                    </div>


                                    <div class="row" style="margin-top:20px; margin-bottom:20px;">
                                        <div class="col-md-8 col-md-offset-2">
                                            <a class="btn btn-block add_additional_bonus_programs">Add another bonus program</a>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <button type="submit" class="btn btn-block btn-primary">Submit</button>
                                    </div>
                                </div>
                            </div>
                            <?php echo Form::close(); ?>


                            
                                
                                    
                                
                                
                                    
                                
                            

                            <div>
                                <span id="connected_to_awardwallet_info">*To help us plan your trip, you can connect AwardWallet to AwardCat so we can see your miles and points balances. We will not have access to your passwords.</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>

    <script src="<?php echo e(asset('assets/select2-4.0.3/dist/js/select2.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/selectize.js/dist/js/standalone/selectize.js')); ?>"></script>

    <script type="text/javascript">

        //SELECT FROM
        $('.selectize-form-search').selectize({
            valueField: 'display_name',
            labelField: 'code',
            selectOnTab: 'true',
            searchField: ['code', 'display_name'],
            optgroupValueField: 'id',
            optgroupField: 'id',
            sortField: 'sort',
            create: false,
            render: {
                option: function(item, escape) {
                    var bonus_programs = [];
                    for (var i = 0, n = item.length; i < n; i++) {
                        bonus_programs.push('<span>' + escape(item.display_name) +' (' + escape(item.code) +')</span>');
                    }

                    var str = '<div>' +
                        '<span>' + escape(item.display_name)+', '+ item.code +'</span>' +
                        '</div>';

                    return str;
                }
            },
            load: function(query, callback) {
                if (!query.length) return callback();

                var url = "<?php echo e(route('bonus_program.search')); ?>";
                var token = "<?php echo e(csrf_token()); ?>";

                $.ajax({
                    url: url,
                    type: 'POST',
                    data: {q: query,_token:token,
                    },
                    error: function() {
                        callback();
                    },
                    success: function(res) {
                        console.log(res);
                        callback(res);
                    }
                });
            }
        });


        //Select2
//        $(document).ready(function() {
//            $(".select_bonus_programs").select2();
//        });

        //Add additional bonus programs
        var i = 0;
        <?php if(isset(Session::all()['AwardWallet']['account']) && count(Session::all()['AwardWallet']['account']) > 0): ?>
            <?php $__currentLoopData = Session::all()['AwardWallet']['account']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                if(i < 9)
                {
                    $('#additional_bonus_programs').before(
                        '<div class="row" id="main_bonus_programs' + i + '" style="margin-top:20px; margin-bottom:20px;">' +
                            '<div class="col-md-6">' +
                                '<select id="main_bonus_programs_search' + i + '" class="form-control select_bonus_programs selectpicker" name="bonus_programs[]">' +
                                    '<option value="<?php echo e(Session::all()['AwardWallet']['account'][($loop->iteration - 1)]['displayName']); ?>"><?php echo e(Session::all()['AwardWallet']['account'][($loop->iteration - 1)]['displayName']); ?></option>' +
                                '</select>' +
                            '</div>' +
                            '<div class="col-md-6">' +
                                '<input type="number" class="form-control" name="number_of_points[]" placeholder="Number of Points" value="<?php echo e(Session::all()['AwardWallet']['account'][($loop->iteration - 1)]['balanceRaw']); ?>" required />' +
                            '</div>' +
                        '</div>'
                    );
                    i++;
                }
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>

        $('.add_additional_bonus_programs').on('click', function(event){

            event.preventDefault();
            if(i < 9){
                $('#additional_bonus_programs').before(
                    '<div class="row" id="main_bonus_programs' + i + '" style="margin-top:20px; margin-bottom:20px;">' +
                        '<div class="col-md-6">' +
                            '<div class="form-group control-group">' +
                                '<select id="main_bonus_programs_search' + i + '" class="form-control selectize-form-search" name="bonus_programs[]" ></select>' +
                            '</div>' +
                        '</div>' +
                        '<div class="col-md-6">' +
                            '<input type="number" class="form-control" name="number_of_points[]" placeholder="Number of Points" value="" required />' +
                        '</div>' +
                    '</div>'
                );

                $("#main_bonus_programs_search" + i).selectize({
                    valueField: 'display_name',
                    labelField: 'display_name',
                    selectOnTab: 'true',
                    searchField: ['code', 'display_name'],
                    optgroupValueField: 'id',
                    optgroupField: 'id',
                    sortField: 'sort',
                    create: false,
                    render: {
                        option: function(item, escape) {
                            var bonus_programs = [];
                            for (var i = 0, n = item.length; i < n; i++) {
                                bonus_programs.push('<span>' + escape(item.display_name) +' (' + escape(item.code) +')</span>');
                            }

                            var str = '<div>' +
                                '<span>' + escape(item.display_name)+', '+ item.code +'</span>' +
                                '</div>';

                            return str;
                        }
                    },
                    load: function(query, callback) {
                        if (!query.length) return callback();

                        var url = "<?php echo e(route('bonus_program.search')); ?>";
                        var token = "<?php echo e(csrf_token()); ?>";

                        $.ajax({
                            url: url,
                            type: 'POST',
                            data: {q: query,_token:token,
                            },
                            error: function() {
                                callback();
                            },
                            success: function(res) {
                                console.log(res);
                                callback(res);
                            }
                        });
                    }
                });

                $('#main_bonus_programs_search2').css("color","red");

                i++;
            }
        });
    </script>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('stylesheets'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('assets/select2-4.0.3/dist/css/select2.min.css')); ?>"/>
    <link rel="stylesheet" href="<?php echo e(asset('assets/selectize.js/dist/css/selectize.bootstrap3.css')); ?>"/>

    <style>
        .select2-container--default .select2-selection--single{
            font-weight: 300;
            border-color: #ecf0f1;
            background-color: #ecf0f1;
            border-radius: 0px;
            -webkit-border-radius: 0px;
            -moz-border-radius: 0px;
            box-shadow: none;
            -webkit-box-shadow: none;
            -moz-box-shadow: none;
            -moz-transition: all .2s;
            -o-transition: all .2s;
            -webkit-transition: all .2s;
            transition: all .2s;
            display: block;
            width: 100%;
            height: 34px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
        }
        .select2-selection__rendered{
            margin-top:-4px;
        }
        .select2-selection__arrow{
            margin-top:4px;
        }
        .wrapsection{
            padding-top: 150px;
        }

        /* award wallet help info */
        #awardwallet_helper_info{
            display: none;
            position: absolute;
        }

        #awardwallet_helper_info_icon{
            position: relative;
        }

        #awardwallet_helper_info_icon:hover #awardwallet_helper_info{
            display: block;
        }

        #connected_to_awardwallet_info{
            font-size: 12px;
        }
    </style>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('user.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>