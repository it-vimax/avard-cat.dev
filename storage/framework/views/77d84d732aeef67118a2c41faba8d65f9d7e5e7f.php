<?php $__env->startSection('title', 'Step 2'); ?>
<?php $__env->startSection('description', 'Service for spending points.'); ?>

<?php $__env->startSection('content'); ?>
    <section id="form-section" class="parallax section" style="background-image: url('/img/1.jpg');">
        <div class="wrapsection">
            <div class="parallax-overlay" style="background-color: #0023f2;opacity:0.9;"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Title -->
                        <div class="maintitle">
                            <h3 class="section-title">Step 2. Some text here.</h3>
                            <p class="lead" style="margin-bottom:20px;">
                                Just few steps to finish...
                            </p>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-12 col-md-offset-2">
                        <div class="service-box wow zoomIn" data-wow-duration="1.5s" data-wow-delay="0.1s">
                            <h4>Estimated cost using points:</h4>
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3">
                                    <div class="well" style="text-align: center">
                                        <strong style="font-size:1.5em;"><?php echo e($zone_price); ?></strong>
                                        <div>
                                            <small>For <?php echo e(session('adults')); ?> <?php echo e((session('adults') > 1)?'persons':'person'); ?> in
                                            <?php if(session('trip_type') == 'Round-trip' || session('trip_type') == 'Multi-city'): ?>
                                                both ways
                                            <?php elseif(session('trip_type') == 'One-way'): ?>
                                                one way
                                            <?php endif; ?>
                                             in <?php echo e(session('class')); ?> class.</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <?php echo Form::open(['route' => 'stepThree', 'method'=>'POST']); ?>

                                <div class="col-md-6 col-md-offset-3">
                                    <button type="submit" class="btn btn-block btn-primary">Buy for Points</button>
                                </div>
                                <?php echo Form::close(); ?>

                            </div>

                            <h4 style="margin-top: 20px;">Good flights available starting from:*</h4>
                            <div id="info-find">
                            <div id="all_airlines">
                                <div id="img_loading_airlines"></div>
                            </div>

                            <div id="global-cont"></div>


                        </div>
                        <div>
                            <span id="good_flights_info">*"Good flights" - no more than one connection, leave no earlier than 07:00, leave no later than 21:00</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>

    <script type="text/javascript">

        //Price for Airlines
        var token = "<?php echo e(csrf_token()); ?>";
        var url = "<?php echo e(route('stepTwoBackground')); ?>";
        $.ajax({
            type: "POST",
            url: url,
            data: {_token:token},
            success: function(data) {
                console.log(data);
                if(data.success == 'false'){
                    $('#info-find').html('<p>Flights not found. Please try a another date</p>');
                }
                $('#global-cont').html(data);

                $('#all_airlines').hide();
                $('.price_for_airlines_container').show();

            }
        });

        $(document).on('click','#more-info-flight', function(){
            $('#fares-container').show();
        })

        //Getting Airlines
        
        
        
            
            
            
            
                
                
                    
                        
                    
                
                
            
        

        
        
        
            
                
                    
                
                
                    
                
            
            
        

    </script>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('stylesheets'); ?>
    <style>
        #parallax{
            display: none;
        }
        #show_more_arln{
            display: none;
        }
        #img_loading_airlines{
            display: block;
            margin:40px auto 30px;
            padding:0px;
            width:100px;
            height:100px;
            background: url('/img/Loading_icon.gif');
            background-position: center;

        }
        .price_for_airlines_container{
            display:none;
        }
        #fares-container{
            display:none;
        }
        .wrapsection{
            padding-top: 150px;
        }
        #good_flights_info{
            font-size: 12px;
        }
    </style>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('user.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>