<?php $__env->startSection('title', 'Step 3'); ?>
<?php $__env->startSection('description', 'Service for spending points.'); ?>

<?php $__env->startSection('content'); ?>
    <section id="form-section" class="parallax section" style="background-image: url('/img/1.jpg');">
        <div class="wrapsection">
            <div class="parallax-overlay" style="background-color: #0023f2;opacity:0.9;"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Title -->
                        <div class="maintitle">
                            <h3 class="section-title">Authorization.</h3>
                            <p class="lead" style="margin-bottom:20px;">
                                Do we need some additional text
                            </p>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-12 col-md-offset-2">
                        <div class="service-box wow zoomIn" data-wow-duration="1.5s" data-wow-delay="0.1s">


                            <h4>How much bonus points you have?</h4>
                            <?php echo Form::open(['route' => 'stepFour', 'method'=>'POST']); ?>


                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="basic-addon1">@</span>
                                        <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
                                    </div>
                                    <br>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" name="last_name" id="last_name">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-block btn-primary">Authorization</button>
                                </div>
                            </div>

                            <?php echo Form::close(); ?>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>

    <script src="<?php echo e(asset('assets/select2-4.0.3/dist/js/select2.min.js')); ?>"></script>

    <script type="text/javascript">

        //Select2
//        $(document).ready(function() {
//            $(".select_bonus_programs").select2();
//        });

        //Add additional bonus programs
        var i = 0;

        $('.add_additional_bonus_programs').on('click', function(event){
            event.preventDefault();
            if(i < 9){
                $('#main_bonus_programs').clone().appendTo('#additional_bonus_programs');
                i++;
            }
        });
    </script>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('stylesheets'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('assets/select2-4.0.3/dist/css/select2.min.css')); ?>"/>
    <style>
        .select2-container--default .select2-selection--single{
            font-weight: 300;
            border-color: #ecf0f1;
            background-color: #ecf0f1;
            border-radius: 0px;
            -webkit-border-radius: 0px;
            -moz-border-radius: 0px;
            box-shadow: none;
            -webkit-box-shadow: none;
            -moz-box-shadow: none;
            -moz-transition: all .2s;
            -o-transition: all .2s;
            -webkit-transition: all .2s;
            transition: all .2s;
            display: block;
            width: 100%;
            height: 34px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
        }
        .select2-selection__rendered{
            margin-top:-4px;
        }
        .select2-selection__arrow{
            margin-top:4px;
        }
        .wrapsection{
            padding-top: 150px;
        }
    </style>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('user.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>