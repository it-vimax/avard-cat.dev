<!doctype html>
<html lang="en">

<?php echo $__env->make('user.partials._head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<body>

    <div id="parallax">
    </div>

    <?php echo $__env->make('user.partials._header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <?php echo $__env->yieldContent('content'); ?>

    <?php echo $__env->make('user.partials._footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <?php echo $__env->make('user.partials._javascript', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

</body>
</html>