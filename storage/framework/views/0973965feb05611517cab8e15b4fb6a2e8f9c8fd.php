<?php $__env->startSection('title', 'Step 4'); ?>
<?php $__env->startSection('description', 'Service for spending points.'); ?>

<?php $__env->startSection('content'); ?>
    <section id="form-section" class="parallax section" style="background-image: url('/img/1.jpg');">
        <div class="wrapsection">
            <div class="parallax-overlay" style="background-color: #0023f2;opacity:0.9;"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Title -->
                        <div class="maintitle">
                            <h3 class="section-title">Sorry, you have not enought Points</h3>
                            <p class="lead" style="margin-bottom:20px;">
                                But we have something for you...
                            </p>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-12 col-md-offset-2">
                        <div class="service-box wow zoomIn" data-wow-duration="1.5s" data-wow-delay="0.1s">
                            <h4>Use this link to earn more points</h4>
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3">
                                    <div class="form-group">
                                        <a href="https://www.google.com.ua/" class="btn btn-block btn-primary" target="_blank">Get more points</a>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('stylesheets'); ?>
    <style>
        .wrapsection{
            padding-top: 150px;
        }
    </style>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('user.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>