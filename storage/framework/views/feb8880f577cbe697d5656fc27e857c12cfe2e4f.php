<?php $__env->startSection('title', 'Admin Dashboard'); ?>

<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light portlet-fit portlet-datatable ">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-layers font-green"></i>
                    <span class="caption-subject font-green sbold uppercase">Trips</span>
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="dataTable">
                    <thead>
                    <tr>
                        <th> # </th>
                        <th> Tprip Type </th>
                        <th> Name </th>
                        <th> Email </th>
                        <th> Status </th>
                        <th> Create at </th>
                        <th> More detals </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $__currentLoopData = $trips; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $trip): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr class="odd gradeX"  <?php if($trip->flexible_checked): ?>
                                                    style="color: #0000ff"
                                                <?php endif; ?>>
                            <td> <?php echo e($loop->iteration); ?></td>
                            <td> <?php echo e($trip->trip_type); ?> </td>
                            <td><?php echo e($trip->client->first_name); ?></td>
                            <td>
                                <a href="mailto:<?php echo e($trip->client->user->email); ?>"><?php echo e($trip->client->user->email); ?></a>
                            </td>
                            <td>
                                <div class="form-inline">
                                    <select name="status" class="form-control select-status" data-trip-id="<?php echo e($trip->id); ?>">
                                        <option <?php echo e(($trip->status == 'New')?'selected':''); ?> value="New">New</option>
                                        <option <?php echo e(($trip->status == 'In Process')?'selected':''); ?> value="In Process">In Process</option>
                                        <option <?php echo e(($trip->status == 'Closed')?'selected':''); ?> value="Closed">Closed</option>
                                    </select>
                               </div>

                            </td>
                            <td>
                                <span><?php echo e($trip->created_at->format('m-d-Y')); ?></span>
                            </td>
                            <td>
                                <span> <a href="<?php echo e(asset('/admin/trip/' . $trip->id)); ?>">Show</a></span>
                            </td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
    <script src="<?php echo e(asset('/assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('/assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            table = $('#dataTable').DataTable({
                "pageLength": 50
            });
        });

        $('.select-status').on('change', function()
        {
            var status = $(this).val();
            var trip_id = $(this).attr("data-trip-id");
            var token = "<?php echo e(csrf_token()); ?>";
            var url = "<?php echo e(route('admin.trip.update.status')); ?>";
            $.ajax({
                type: "POST",
                url: url,
                data: {status: status, trip_id: trip_id, _token:token},
                success: function()
                {
                    $('#status-saved-' + trip_id).show().css({
                        "opacity" : "1",
                        "transition" : "opacity 2s ease-in-out"
                    });
                }
            });
        });
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('stylesheets'); ?>
    <link href="<?php echo e(asset('/assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>