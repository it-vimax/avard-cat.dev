<?php $__env->startSection('title', 'Admin Dashboard'); ?>

<?php $__env->startSection('content'); ?>
<div class="row">


    <div class="col-md-8">
        <div class="portlet box blue-hoki">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>Portlet1 </div>
                <div class="actions">
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-pencil"></i> Edit </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-plus"></i> Add </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-scrollable">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th> # </th>
                            <th> Trip ID </th>
                            <th> Trip Type </th>
                            <th> From </th>
                            <th> To </th>
                            <th> Departure Date </th>
                            <th> Return Date </th>
                            <th> Adults </th>
                            <th> Class </th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php $__currentLoopData = $trip->direction; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $direction): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td> <?php echo e($loop->iteration); ?> </td>
                                <td> <?php echo e($direction->trip_id); ?> </td>
                                <td> <?php echo e($direction->trip_type); ?> </td>
                                <td> <?php echo e($direction->from); ?> </td>
                                <td> <?php echo e($direction->to); ?> </td>
                                <td> <?php echo e($direction->departure_date); ?> </td>
                                <td> <?php echo e($direction->return_date); ?> </td>
                                <td> <?php echo e($direction->adults); ?> </td>
                                <td> <?php echo e($direction->class); ?> </td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="portlet box blue-hoki">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>Portlet1 </div>
                <div class="actions">
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-pencil"></i> Edit </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-plus"></i> Add </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-scrollable">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th> UserID </th>
                            <th> First Name </th>
                            <th> Last Name </th>
                            <th> Street Address 1 </th>
                            <th> Street Address 2 </th>
                            <th> City </th>
                            <th> ZIP </th>
                            <th> Country </th>
                            <th> Mobile phone </th>
                            <th> Bussines phone </th>
                            <th> Email </th>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <td> <?php echo e($trip->client->user->id); ?> </td>
                            <td> <?php echo e($trip->client->first_name); ?> </td>
                            <td> <?php echo e($trip->client->last_name); ?> </td>
                            <td> <?php echo e($trip->client->street_address_line_1); ?> </td>
                            <td> <?php echo e($trip->client->street_address_line_2); ?> </td>
                            <td> <?php echo e($trip->client->city_town_department); ?> </td>
                            <td> <?php echo e($trip->client->zip_personal_code); ?> </td>
                            <td> <?php echo e($trip->client->country); ?> </td>
                            <td> <?php echo e($trip->client->mobile_phone_number); ?> </td>
                            <td> <?php echo e($trip->client->business_phone_number); ?> </td>
                            <td> <?php echo e($trip->client->user->email); ?> </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('stylesheets'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>