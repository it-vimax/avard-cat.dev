<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Direction extends Model
{
    protected $table = 'directions';

    public function trip()
    {
        return $this->belongsTo('App\Trip');
    }
}
