<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bonus extends Model
{
    protected $table = 'bonuses';

    public function trip()
    {
        return $this->belongsTo('App\Trip');
    }
}
