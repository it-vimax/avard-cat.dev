<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public function zone()
    {
        return $this->belongsTo('App\Zone');
    }

    public function cities()
    {
        return $this->hasMany('App\City');
    }
}
