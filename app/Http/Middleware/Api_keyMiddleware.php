<?php

namespace App\Http\Middleware;

use App\Http\Controllers\AwardWalletController;
use App\Http\Controllers\FormController;
use Closure;

class Api_keyMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->server('HTTP_HOST') == 'robert.dev')
        {
            AwardWalletController::$api_key = '705ef8f9bc3cfaf5793171087bfdac8284c4a31e';
            AwardWalletController::$invittion_key = 'omhtyhbhqa';
            FormController::$language = '&L=ru';
        }
        else
        {
            AwardWalletController::$api_key = '8f5bafb9891556e8a1ee7b57301087cb88be5fb1';
            AwardWalletController::$invittion_key = 'zhomdqxmie';
            FormController::$language = '&L=en';
        }

        return $next($request);
    }
}
