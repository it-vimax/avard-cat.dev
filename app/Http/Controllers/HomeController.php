<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user.pages.home');
    }

    public function role()
    {
        if(Auth::user()->role->name == 'Admin')
        {
            return redirect()->route('admin.index');
        }
    }
}
