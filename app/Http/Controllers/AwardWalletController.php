<?php

namespace App\Http\Controllers;

use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;

class AwardWalletController extends Controller
{
    // key has middlewar
    public static $api_key;
    public static $invittion_key;

    //  maks
    //  '705ef8f9bc3cfaf5793171087bfdac8284c4a31e';
    //  'omhtyhbhqa';

    //  robert
    //  '8f5bafb9891556e8a1ee7b57301087cb88be5fb1';
    //  'zhomdqxmie';

    public static $access = '2';
    private $loyalty_program = 'Airlines';

    public function auth()
    {
        $url = 'https://awardwallet.com/m/connections/approve/' . self::$invittion_key . '/' . self::$access;
        return redirect($url)->header('X-Authentication', self::$api_key);
    }

    public function callback(Request $request)
    {
        $url = "https://business.awardwallet.com/api/export/v1/providers/list";
        $myCurl = curl_init();
        curl_setopt_array($myCurl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1,
        ));
        curl_setopt($myCurl, CURLOPT_HTTPHEADER, array(
            'X-Authentication: ' . self::$api_key
        ));
        $responseJson = curl_exec($myCurl);
        curl_close($myCurl);
        $responseArr = json_decode($responseJson);

//        dump($responseArr);

//        $stringList = '';
//        foreach ($responseArr as $k => $v) {
//            $stringList .= $v->code . '|' . $v->displayName . "\n";
//        }

//        $stringList = '';
//        $stringList .= '<table>';
//        foreach ($responseArr as $k => $v) {
//            $stringList .= '<tr><td>' . $v->code . '</td><td>' . $v->displayName . '</td></tr>';
//        }
//        $stringList .= '</table>';
//
//        echo $stringList;


        $url = "https://business.awardwallet.com/api/export/v1/connectedUser/" . $request->userId;
        $myCurl = curl_init();
        curl_setopt_array($myCurl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1,
        ));
        curl_setopt($myCurl, CURLOPT_HTTPHEADER, array(
            'X-Authentication: ' . self::$api_key
        ));
        $responseJson = curl_exec($myCurl);
        curl_close($myCurl);
        $responseArr = json_decode($responseJson);

        $full_name_arr = explode(" ", $responseArr->fullName);
        // writing user data to a session
        session()->put('AwardWallet.firstName', $full_name_arr[0]);
        session()->put('AwardWallet.lastName', $full_name_arr[1]);
        session()->put('AwardWallet.email', $responseArr->email);

        if(is_array($responseArr->accounts))
        {
            $i=0;
            foreach($responseArr->accounts as $account)
            {
                if($account->kind == $this->loyalty_program)
                {
                    session()->put('AwardWallet.account.' . $i . '.displayName', $account->displayName);
                    session()->put('AwardWallet.account.' . $i . '.balanceRaw', $account->balanceRaw);
                    $i++;
                }
            }
        }
//        session()->flush();
//        dump($responseArr);
//        dump(session()->all());
        return redirect()->route('stepThree');
    }
}
