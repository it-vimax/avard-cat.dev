<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Trip;
use Illuminate\Support\Facades\DB;

class TripController extends Controller
{
    public function index()
    {
        $trips = Trip::orderBy('id', 'DESC')->get();
        return view('admin.trips.index', ['trips' => $trips]);
    }

    public function show($id)
    {
        $trip = Trip::find($id);
        if(isset($trip))
        {
            $locality_directions = [];

            for($i=0; $i<count($trip->directions->toArray()); $i++)
            {
                $options = ['from', 'to'];

                foreach ($options as $option)
                {
                    $airport = DB::table('airports')->where('code', '=', $trip->directions[$i]->{$option})->get();

                    if(count($airport->toArray()) > 0)
                    {
                        $city = DB::table('cities')->where('id', '=', $airport[0]->city_id)->get();
                    }
                    else
                    {
                        $city = DB::table('cities')->where('code', '=', $trip->directions[$i]->{$option})->get();
                    }
                    $country = DB::table('countries')->where('id', '=', $city[0]->country_id)->get();

                    $locality_directions[$option][$i]['city'] = $city->all()[0]->name;
                    $locality_directions[$option][$i]['country'] = $country->all()[0]->name;
                }
            }
            return view('admin.trips.show', ['trip' => $trip, 'locality_directions' => $locality_directions]);
        }
        return redirect()->back();
    }

    public function update_status(Request $request)
    {
        if($request->ajax() && $request->isMethod('POST'))
        {
            $trip = Trip::find($request->trip_id);

            if(isset($trip))
            {
                $trip->status = $request->status;
                $trip->save();
                return json_encode(TRUE);
            }
            return json_encode(FALSE);
        }
        return json_encode(FALSE);
    }
}
