<?php

namespace App\Http\Controllers;

use App\Bonus;
use App\Direction;
use Validator;
use Illuminate\Http\Request;
use App\User;
use App\Client;
use App\Trip;
use Carbon\Carbon;

class PaymentController extends Controller
{

    public function enter(Request $request)
    {
        return view('user.form.enter');
    }

    public function registration(Request $request)
    {
        if($request->isMethod('post'))
        {
            $ruleArr = [
                'first_name'            => 'required|min:3|max:50',
                'last_name'             => 'required|min:3|max:50',
                'street_address_line_1' => 'required|max:50',
                'street_address_line_2' => 'required|max:50',
                'city_town_department'  => 'required|max:50',
                'state_provice_region_country'  => 'required|max:50',
                'zip_personal_code'     => 'required|max:50',
                'mobile_phone_number'   => 'required|max:50',
                'business_phone_number' => 'required|max:50',
                'email_address'         => 'required|string|email|max:50|unique:users,email',
//                'verify_email_address'  => 'required|string|email|max:50|same:email_address',
                'password'              => 'required|min:6|max:50',
                'verify_password'       => 'required|min:6|same:password|max:50',
            ];
            $messageArray = [];
            $validator = Validator::make($request->all(), $ruleArr, $messageArray);
            if($validator->fails())
            {
                return redirect()->route('enter')->withErrors($validator)->withInput();
            }
        }

        $user = new User();
        $user->email = $request->email_address;
        $user->password = bcrypt($request->password);
        $user->role_id = 1;
        $user->save();

        $client = new Client();
        $client->user_id                = $user->id;
        $client->first_name             = $request->first_name;
        $client->last_name              = $request->last_name;
        $client->street_address_line_1  = $request->street_address_line_1;
        $client->street_address_line_2  = $request->street_address_line_2;
        $client->city_town_department   = $request->city_town_department;
        $client->zip_personal_code      = $request->zip_personal_code;
        $client->country                = $request->country;
        $client->mobile_phone_number    = $request->mobile_phone_number;
        $client->business_phone_number  = $request->business_phone_number;
        $client->save();

        $trip = new Trip();
        $trip->client_id = $client->id;
        $trip->trip_type = session('trip_type');
        if(session()->has('flexible'))
        {
            $trip->flexible_checked = TRUE;
            $trip->flexible_date = session()->get('flexible_date');
            $trip->flexible_period = session()->get('period');
        }
        $trip->status = session('status');
        $trip->save();

        if(is_array(session('from')))
        {
            for ($i=0; $i<count(session('from')); $i++)
            {
                $directionModel = new Direction();
                $directionModel->trip_id = $trip->id;
                $directionModel->trip_type = $trip->trip_type;
                $directionModel->from = session('from')[$i];
                $directionModel->to = session('to')[$i];
                $directionModel->departure_date = Carbon::createFromFormat('m-d-Y', session('departure_date')[$i])->format('Y-m-d');

                $return_date = session('return_date');
                if(isset($return_date) && is_array($return_date))
                {
                    $directionModel->return_date = Carbon::createFromFormat('m-d-Y', session('return_date')[$i])->format('Y-m-d');
                }

                $directionModel->adults = session('adults');
                $directionModel->class = session('class');
                $directionModel->save();
            }
        }
        else
        {
            $directionModel = new Direction();
            $directionModel->trip_id = $trip->id;
            $directionModel->trip_type = $trip->trip_type;
            $directionModel->from = session('from');
            $directionModel->to = session('to');
            $directionModel->departure_date = Carbon::createFromFormat('m-d-Y', session('departure_date'))->format('Y-m-d');

            $return_date = session('return_date');
            if(isset($return_date))
            {
                $directionModel->return_date = Carbon::createFromFormat('m-d-Y', session('return_date'))->format('Y-m-d');
            }

            $directionModel->adults = session('adults');
            $directionModel->class = session('class');
            $directionModel->save();
        }

        if(is_array(session('bonus_programs')))
        {
            for($i=0; $i < count(session('bonus_programs')); $i++)
            {
                $bonus = new Bonus();
                $bonus->trip_id = $trip->id;
                $bonus->name_program = session('bonus_programs')[$i];
                $bonus->points = session('number_of_points')[$i];
                $bonus->save();
            }
        }
        else
        {
            $bonus = new Bonus();
            $bonus->trip_id = $trip->id;
            $bonus->name_program = session('bonus_programs');
            $bonus->points = session('number_of_points');
            $bonus->save();
        }

        dump(session()->all());

        return;
    }
}
