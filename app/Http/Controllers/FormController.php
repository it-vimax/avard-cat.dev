<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Airport;
use App\City;
use App\ZonePrice;
use Exception;
use Validator;


class FormController extends Controller
{
    const ROUND_TRIP = 'Round-trip';
    const ONE_WAY = 'One-way';
    const MULTI_CITY = 'Multi-city';
    public static $language;

    public function roundTripStepTwo(Request $request)
    {
        if(!isset($request->flexible))
        {
            $this->validate($request, [
                'departure_date'    => 'required',
                'return_date'       => 'required',
            ]);
        }

        session([
            'trip_type' => self::ROUND_TRIP,
            'from' => $request->select_from,
            'to' => $request->select_to,
            'departure_date' => $request->departure_date,
            'return_date' => $request->return_date,
            'flexible' => $request->flexible,
            'flexible_date' => $request->flexible_date,
            'period' => $request->period,
            'adults' => $request->adults,
            'class' => $request->class,
        ]);

        if(isset($request->flexible))
        {
            session()->forget('departure_date');
            session()->forget('return_date');
        }
        else
        {
            session()->forget('flexible');
            session()->forget('period');
            session()->forget('flexible_date');
        }

        //Country Zone of Airport or City From
        $airport1 = Airport::where('code', $request->select_from)->first();
        if(isset($airport1) && $airport1){
            $zone1_id = $airport1->city->country->zone->id;
            session([
                'from_code' => $request->select_from,
            ]);
        }
        else{
            $city1 = City::where('name', $request->select_from)->first();
            $zone1_id = $city1->country->zone->id;
            //$airport = Airport::where('city_id', $city1->id)->first();
            session([
                'from_code' => $city1->code,
            ]);
        }

        //Country Zone of Airport or City To
        $airport2 = Airport::where('code', $request->select_to)->first();
        if(isset($airport2) && $airport2){
            $zone2_id = $airport2->city->country->zone->id;
            session([
                'to_code' => $request->select_to,
            ]);
        }
        else{
            $city2 = City::where('name', $request->select_to)->first();
            $zone2_id = $city2->country->zone->id;
            //$airport = Airport::where('city_id', $city2->id)->first();
            session([
                'to_code' => $city2->code,
            ]);
        }

        $zone_price_obj = ZonePrice::where('zone1_id', $zone1_id)->where('zone2_id', $zone2_id)->first();
        if(session('class') == 'business'){
            $zone_price = $zone_price_obj->points_business*$request->adults*2;
        }
        elseif(session('class') == 'first'){
            $zone_price = $zone_price_obj->points_first*$request->adults*2;
        }
        elseif(session('class') == 'economy'){
            $zone_price = $zone_price_obj->points_economy*$request->adults*2;
        }


        session([
            'zone_price_total' => $zone_price,
        ]);

        if($request->flexible){
            if(Carbon::now()->format('m-Y') == $request->flexible_date){
                session([
                    'departure_date' => Carbon::now()->addDays(7)->format('m-d-Y'),
                    'return_date' => Carbon::now()->addDays(7+$request->period)->format('m-d-Y'),
                ]);
            }else{
                session([
                    'departure_date' => Carbon::createFromFormat('m-Y', $request->flexible_date)->format('m-d-Y'),
                    'return_date' => Carbon::createFromFormat('m-Y', $request->flexible_date)->addDays($request->period)->format('m-d-Y'),
                ]);
            }

        }

        return view('user.form.step-2')->withZone_price($zone_price);
    }

    public function oneWayStepTwo(Request $request)
    {
        $this->validate($request, [
            'departure_date'    => 'required',
        ]);

        session([
            'trip_type' => self::ONE_WAY,
            'from' => $request->one_way_select_from,
            'to' => $request->one_way_select_to,
            'departure_date' => $request->departure_date,
            'adults' => $request->adults,
            'class' => $request->class,
        ]);

        if(!isset($request->flexible))
        {
            session()->forget('flexible');
            session()->forget('period');
            session()->forget('flexible_date');
        }

        //Country Zone of Airport or City From
        $airport1 = Airport::where('code', $request->one_way_select_from)->first();
        if (isset($airport1) && $airport1) {
            $zone1_id = $airport1->city->country->zone->id;
            session([
                'from_code' => $request->one_way_select_from
            ]);
        }
        else{
            $city1 = City::where('name', $request->one_way_select_from)->first();
            $zone1_id = $city1->country->zone->id;
            //$airport = Airport::where('city_id', $city1->id)->first();
            session([
                'from_code' => $city1->code,
            ]);
        }

        //Country Zone of Airport or City To
        $airport2 = Airport::where('code', $request->one_way_select_to)->first();
        if (isset($airport2) && $airport2) {
            $zone2_id = $airport2->city->country->zone->id;
            session([
                'to_code' => $request->one_way_select_to,
            ]);
        }else{
            $city2 = City::where('name', $request->one_way_select_to)->first();
            $zone2_id = $city2->country->zone->id;
            session([
                'to_code' => $city2->code,
            ]);
        };

        $zone_price_obj = ZonePrice::where('zone1_id', $zone1_id)->where('zone2_id', $zone2_id)->first();;

        if(session('class') == 'business')
        {
            $zone_price = $zone_price_obj->points_business * $request->adults;
        }
        elseif(session('class') == 'first')
        {
            $zone_price = $zone_price_obj->points_first * $request->adults;
        }
        elseif(session('class') == 'economy')
        {
            $zone_price = $zone_price_obj->points_economy * $request->adults;
        }

        session([
            'zone_price_total' => $zone_price,
        ]);

        return view('user.form.step-2', ['zone_price' => $zone_price]);
    }

    public function multiCityStepTwo(Request $request)
    {
        if(is_array($request->departure_date))
        {
            $ruleArr = [];
            for($i=0; $i < count($request->departure_date); $i++)
            {
                $ruleArr[$i] = 'required';
            }
            $messageArray = [];
            $validator = Validator::make($request->all()['departure_date'], $ruleArr, $messageArray);
            if($validator->fails())
            {
                return redirect()->route('home')->withErrors($validator)->withInput();
            }
        }

        session([
            'trip_type' => self::MULTI_CITY,
            'from' => $request->multy_city_select_from,
            'to' => $request->multy_city_select_to,
            'departure_date' => $request->departure_date,
            'adults' => $request->adults,
            'class' => $request->class,
        ]);

        if(!isset($request->flexible))
        {
            session()->forget('flexible');
            session()->forget('period');
            session()->forget('flexible_date');
        }

        $countDestination = count($request->multy_city_select_from);
        $from_arr = session('from');
        $to_arr = session('to');
//        $departure_date_arr = session('departure_date');

        $from_code_arr = [];
        $to_code_arr = [];
        $zone_price_arr = [];

        for($i=0; $i < $countDestination; $i++)
        {
            //Country Zone of Airport or City From
            $airport1 = Airport::where('code', $from_arr[$i])->first();
            if (isset($airport1) && $airport1) {
                $zone1_id = $airport1->city->country->zone->id;
                $from_code_arr[] = $from_arr[$i];
            }
            else{
                $city1 = City::where('name', $from_arr[$i])->first();
                $zone1_id = $city1->country->zone->id;
                $from_code_arr[] = $city1->code;
            }

            //Country Zone of Airport or City To
            $airport2 = Airport::where('code', $to_arr[$i])->first();
            if (isset($airport2) && $airport2) {
                $zone2_id = $airport2->city->country->zone->id;
                $to_code_arr[] = $to_arr[$i];
            }else{
                $city2 = City::where('name', $to_arr[$i])->first();
                $zone2_id = $city2->country->zone->id;
                $to_code_arr[] = $city2->code;
            };

            $zone_price_obj = ZonePrice::where('zone1_id', $zone1_id)->where('zone2_id', $zone2_id)->first();;

            if(session('class') == 'business')
            {
                $zone_price_arr[] = $zone_price_obj->points_business * $request->adults;
            }
            elseif(session('class') == 'first')
            {
                $zone_price_arr[] = $zone_price_obj->points_first * $request->adults;
            }
            elseif(session('class') == 'economy')
            {
                $zone_price_arr[] = $zone_price_obj->points_economy * $request->adults;
            }
        }

        session([
            'from_code' => $from_code_arr,
            'to_code' => $to_code_arr,
        ]);

        $zone_price_sum = 0;
        for($i=0; $i < count($zone_price_arr); $i++)
        {
            $zone_price_sum += $zone_price_arr[$i];
        }

        session([
            'zone_price_total' => $zone_price_sum,
        ]);

        return view('user.form.step-2', ['zone_price' => $zone_price_sum]);
    }

    public function stepThree(Request $request)
    {
        session([
            'buy_for_points' => 'true',
        ]);

        return view('user.form.step-3');
    }

    public function stepFour(Request $request)
    {
        session([
            'bonus_programs' => $request->bonus_programs,
            'number_of_points' => $request->number_of_points,
            'status' => 'New',
        ]);
        $total_number_of_points = 0;

        foreach ($request->number_of_points as $key=>$number_of_points){
            $total_number_of_points += $number_of_points;
        }

        if($total_number_of_points >= session('zone_price_total')){
            return view('user.form.step-4-success');
        }
        else{
            return view('user.form.step-4-error');
        }
    }

    public function stepTwoBackground(Request $request)
    {
        if(session('trip_type') == self::ROUND_TRIP)
        {
            try {
                $departure_day = Carbon::createFromFormat('m-d-Y', session('departure_date'))->format('d');
                $departure_month = Carbon::createFromFormat('m-d-Y', session('departure_date'))->format('m');
                $return_day = Carbon::createFromFormat('m-d-Y', session('return_date'))->format('d');
                $return_month = Carbon::createFromFormat('m-d-Y', session('return_date'))->format('m');
                if(session('class') == 'business' || session('class') == 'first'){
                    $class = 'B';
                }else{
                    $class = 'E';
                }

                //Request to Api
                $url = 'http://api.anywayanyday.com/api/NewRequestRedirect/?Route='.$departure_day.''.$departure_month.''.session('from_code').''.session('to_code').''.$return_day.''.$return_month.''.session('to_code').''.session('from_code').'&AD='.session('adults').'&CN=0&SC='.$class.'&Partner=testapic'.self::$language;
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch,CURLOPT_ENCODING,'gzip,deflate');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_URL, $url);
                $data = curl_exec($ch);
                curl_close($ch);
                $xml = simplexml_load_string($data);

                //Start Search
                $completed = 0;
                while ($completed < 100) {
                    $url = 'http://api.anywayanyday.com/api/RequestState/?R='.$xml->attributes()['IdSynonym'];
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_setopt($ch,CURLOPT_ENCODING,'gzip,deflate');
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_URL, $url);
                    $data = curl_exec($ch);
                    curl_close($ch);
                    $xml_completed = simplexml_load_string($data);
                    $completed = ($xml_completed->attributes()['Completed']);
                    sleep(2);
                }

                //$R = ''.$xml->attributes()['IdSynonym'].'';

                //Info from request
                $url = 'http://api.anywayanyday.com/api/Fares/?V=Matrix&R='.$xml->attributes()['IdSynonym'].'&L=EN&PS=1&PN=1&C=USD&S=Time&BC1=0;1&DT1=M;D;E&DT2=M;D;E\'';
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch,CURLOPT_ENCODING,'gzip,deflate');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_URL, $url);
                $data = curl_exec($ch);
                curl_close($ch);
                $xml = simplexml_load_string($data);

                //dd($data);

                $output = array();

                $i = 0;
                foreach ($xml->Arln as $Arln){
                    foreach ($Arln->Fare as $Fare){
                        $price_for_airlines = intval($Fare->attributes()['AT']);
                        if($i == 0 || ($price_for_airlines < $output['price_for_airlines'])){
                            $output['price_for_airlines'] = intval($Fare->attributes()['AT']);
                            $F = ''.$Fare->attributes()['Id'].'';
                            $j = 0;
                            foreach($Fare->Dir as $Dir){
                                $name = ''.$Arln->attributes()['N'].'';
                                $price = ''.$Fare->attributes()['AT'].'';
                                $from = ''.$Dir->attributes()['DepApt'].'';
                                $to = ''.$Dir->attributes()['ArrApt'].'';
                                //From
                                $airlines[$name][$j]['from'] = $from;
                                $airport_from = Airport::where('code', $from)->first();
                                $airlines[$name][$j]['from_city'] = $airport_from->city->name;
                                $airlines[$name][$j]['dep_time'] = explode(";", ''.$Dir->attributes()['DepTm'].'');
                                //To
                                $airlines[$name][$j]['to'] = $to;
                                $airport_to = Airport::where('code', $to)->first();
                                $airlines[$name][$j]['to_city'] = $airport_to->city->name;
                                $airlines[$name][$j]['arr_time'] = explode(";", ''.$Dir->attributes()['ArrTm'].'');
                                //Time of flight
                                $airlines[$name][$j]['time_of_flight'] = $Dir->attributes()['Hr'].':'.$Dir->attributes()['Min'];
                                //Class
                                $airlines[$name][$j]['class'] = session('class');
                                //Price
                                $airlines[$name][$j]['price'] = $price;
                                $j++;
                            }
                        }
                        $i++;
                    }
                    break;
                }


                //URL TO REDIRECT
//            $url = 'http://api.anywayanyday.com/api/GetCreateOrderURL/?R='.$R.'&F='.$F.'&V=0;1&L=EN&C=USD';
//            $ch = curl_init();
//            curl_setopt($ch, CURLOPT_HEADER, 0);
//            curl_setopt($ch,CURLOPT_ENCODING,'gzip,deflate');
//            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//            curl_setopt($ch, CURLOPT_URL, $url);
//            $data = curl_exec($ch);
//            curl_close($ch);
//            $xml = simplexml_load_string($data);

                //Changing URL to give ability for client to choose the AIRLINE


//            $url = ''.$xml->attributes()['URL'].'';
//            $output['url'] = str_replace("makeorder", "offers", $url);
                $output['url'] = 'http://api.anywayanyday.com/api/NewRequestRedirect/?Route='.$departure_day.''.$departure_month.''.session('from_code').''.session('to_code').''.$return_day.''.$return_month.''.session('to_code').''.session('from_code').'&AD='.session('adults').'&CN=0&SC='.$class.'&Partner=testapic'.self::$language;
                $output['success'] = 'true';

                return view('user.form.step-2-background')->withAirlines($airlines)->withOutput($output);

            } catch (Exception $e) {
                //Return Price from DB
                $output['success'] = 'false';
                $output['price_for_airlines'] = '$100';
                $output['url'] = 'https://www.anywayanyday.com/';
                return $output;
            }
        }
        elseif(session('trip_type') == self::ONE_WAY)
        {
            try {
                $departure_day = Carbon::createFromFormat('m-d-Y', session('departure_date'))->format('d');
                $departure_month = Carbon::createFromFormat('m-d-Y', session('departure_date'))->format('m');
                if(session('class') == 'business' || session('class') == 'first'){
                    $class = 'B';
                }else{
                    $class = 'E';
                }

                //Request to Api
                $url = 'http://api.anywayanyday.com/api/NewRequestRedirect/?Route='.$departure_day.''.$departure_month.''.session('from_code').''.session('to_code').''.'&AD='.session('adults').'&CN=0&SC='.$class.'&Partner=testapic'.self::$language;

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch,CURLOPT_ENCODING,'gzip,deflate');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_URL, $url);
                $data = curl_exec($ch);
                curl_close($ch);
                $xml = simplexml_load_string($data);
                //Start Search
                $completed = 0;
                while ($completed < 100) {
                    $url = 'http://api.anywayanyday.com/api/RequestState/?R='.$xml->attributes()['IdSynonym'];
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_setopt($ch,CURLOPT_ENCODING,'gzip,deflate');
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_URL, $url);
                    $data = curl_exec($ch);
                    curl_close($ch);
                    $xml_completed = simplexml_load_string($data);
                    $completed = ($xml_completed->attributes()['Completed']);
                    sleep(2);
                }

                //$R = ''.$xml->attributes()['IdSynonym'].'';

                //Info from request
                $url = 'http://api.anywayanyday.com/api/Fares/?V=Matrix&R='.$xml->attributes()['IdSynonym'].'&L=EN&PS=1&PN=1&C=USD&S=Time&BC1=0;1&DT1=M;D;E&DT2=M;D;E\'';
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch,CURLOPT_ENCODING,'gzip,deflate');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_URL, $url);
                $data = curl_exec($ch);
                curl_close($ch);
                $xml = simplexml_load_string($data);

                //dd($data);

                $output = array();

                $i = 0;
                foreach ($xml->Arln as $Arln){
                    foreach ($Arln->Fare as $Fare){
                        $price_for_airlines = intval($Fare->attributes()['AT']);
                        if($i == 0 || ($price_for_airlines < $output['price_for_airlines'])){
                            $output['price_for_airlines'] = intval($Fare->attributes()['AT']);
                            $F = ''.$Fare->attributes()['Id'].'';
                            $j = 0;
                            foreach($Fare->Dir as $Dir){
                                $name = ''.$Arln->attributes()['N'].'';
                                $price = ''.$Fare->attributes()['AT'].'';
                                $from = ''.$Dir->attributes()['DepApt'].'';
                                $to = ''.$Dir->attributes()['ArrApt'].'';
                                //From
                                $airlines[$name][$j]['from'] = $from;
                                $airport_from = Airport::where('code', $from)->first();
                                $airlines[$name][$j]['from_city'] = $airport_from->city->name;
                                $airlines[$name][$j]['dep_time'] = explode(";", ''.$Dir->attributes()['DepTm'].'');
                                //To
                                $airlines[$name][$j]['to'] = $to;
                                $airport_to = Airport::where('code', $to)->first();
                                $airlines[$name][$j]['to_city'] = $airport_to->city->name;
                                $airlines[$name][$j]['arr_time'] = explode(";", ''.$Dir->attributes()['ArrTm'].'');
                                //Time of flight
                                $airlines[$name][$j]['time_of_flight'] = $Dir->attributes()['Hr'].':'.$Dir->attributes()['Min'];
                                //Class
                                $airlines[$name][$j]['class'] = session('class');
                                //Price
                                $airlines[$name][$j]['price'] = $price;
                                $j++;
                            }
                        }
                        $i++;
                    }
                    break;
                }

                $output['success'] = 'true';
                $output['url'] =   'http://api.anywayanyday.com/api/NewRequestRedirect/?Route='.$departure_day.''.$departure_month.''.session('from_code').''.session('to_code').''.'&AD='.session('adults').'&CN=0&SC='.$class.'&Partner=testapic'.self::$language;

                return view('user.form.step-2-background')->withAirlines($airlines)->withOutput($output);
            } catch (Exception $e) {
                //Return Price from DB
                $output['success'] = 'false';
                $output['price_for_airlines'] = '$100';
                $output['url'] = 'https://www.anywayanyday.com/';
                return $output;
            }
        }
        elseif(session('trip_type') == self::MULTI_CITY)
        {
            // MULTI-CITY
            try {
                $departure_date_arr = session('departure_date');
                $from_code_arr = session('from_code');
                $to_code_arr = session('to_code');
                $partUrl = '';

                for($i=0; $i<count($departure_date_arr); $i++)
                {
                    $departure_day = Carbon::createFromFormat('m-d-Y', $departure_date_arr[$i])->format('d');
                    $departure_month = Carbon::createFromFormat('m-d-Y', $departure_date_arr[$i])->format('m');

                    $partUrl .= $departure_day;
                    $partUrl .= $departure_month;
                    $partUrl .= $from_code_arr[$i];
                    $partUrl .= $to_code_arr[$i];
                }

                if(session('class') == 'business' || session('class') == 'first'){
                    $class = 'B';
                }else{
                    $class = 'E';
                }

                //Request to Api
                $url = 'http://api.anywayanyday.com/api/NewRequestRedirect/?Route='.$partUrl.''.'&AD='.session('adults').'&CN=0&SC='.$class.'&Partner=testapic'.self::$language;

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch,CURLOPT_ENCODING,'gzip,deflate');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_URL, $url);
                $data = curl_exec($ch);
                curl_close($ch);
                $xml = simplexml_load_string($data);
                //Start Search
                $completed = 0;
                while ($completed < 100) {
                    $url = 'http://api.anywayanyday.com/api/RequestState/?R='.$xml->attributes()['IdSynonym'];
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_setopt($ch,CURLOPT_ENCODING,'gzip,deflate');
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_URL, $url);
                    $data = curl_exec($ch);
                    curl_close($ch);
                    $xml_completed = simplexml_load_string($data);
                    $completed = ($xml_completed->attributes()['Completed']);
                    sleep(2);
                }

                //$R = ''.$xml->attributes()['IdSynonym'].'';

                //Info from request
                $url = 'http://api.anywayanyday.com/api/Fares/?V=Matrix&R='.$xml->attributes()['IdSynonym'].'&L=EN&PS=1&PN=1&C=USD&S=Time&BC1=0;1&DT1=M;D;E&DT2=M;D;E\'';
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch,CURLOPT_ENCODING,'gzip,deflate');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_URL, $url);
                $data = curl_exec($ch);
                curl_close($ch);
                $xml = simplexml_load_string($data);

                $output = array();

                $i = 0;
                foreach ($xml->Arln as $Arln){
                    foreach ($Arln->Fare as $Fare){
                        $price_for_airlines = intval($Fare->attributes()['AT']);
                        if($i == 0 || ($price_for_airlines < $output['price_for_airlines'])){
                            $output['price_for_airlines'] = intval($Fare->attributes()['AT']);
                            $F = ''.$Fare->attributes()['Id'].'';
                            $j = 0;
                            foreach($Fare->Dir as $Dir){
                                $name = ''.$Arln->attributes()['N'].'';
                                $price = ''.$Fare->attributes()['AT'].'';
                                $from = ''.$Dir->attributes()['DepApt'].'';
                                $to = ''.$Dir->attributes()['ArrApt'].'';
                                //From
                                $airlines[$name][$j]['from'] = $from;
                                $airport_from = Airport::where('code', $from)->first();
                                $airlines[$name][$j]['from_city'] = $airport_from->city->name;
                                $airlines[$name][$j]['dep_time'] = explode(";", ''.$Dir->attributes()['DepTm'].'');
                                //To
                                $airlines[$name][$j]['to'] = $to;
                                $airport_to = Airport::where('code', $to)->first();
                                $airlines[$name][$j]['to_city'] = $airport_to->city->name;
                                $airlines[$name][$j]['arr_time'] = explode(";", ''.$Dir->attributes()['ArrTm'].'');
                                //Time of flight
                                $airlines[$name][$j]['time_of_flight'] = $Dir->attributes()['Hr'].':'.$Dir->attributes()['Min'];
                                //Class
                                $airlines[$name][$j]['class'] = session('class');
                                //Price
                                $airlines[$name][$j]['price'] = $price;
                                $j++;
                            }
                        }
                        $i++;
                    }
                    break;
                }

                $output['success'] = 'true';
                $output['url'] =   'http://api.anywayanyday.com/api/NewRequestRedirect/?Route='.$partUrl.''.'&AD='.session('adults').'&CN=0&SC='.$class.'&Partner=testapic'.self::$language;

                return view('user.form.step-2-background')->withAirlines($airlines)->withOutput($output);
            } catch (Exception $e) {
                //Return Price from DB
                $output['success'] = 'false';
                $output['price_for_airlines'] = '$100';
                $output['url'] = 'https://www.anywayanyday.com/';
                return $output;
            }
        }
    }

    //Not needed
    public function roundTripStepTwoBackgroundOld(Request $request)
    {
        $departure_day = Carbon::createFromFormat('m-d-Y', session('departure_date'))->format('d');
        $departure_month = Carbon::createFromFormat('m-d-Y', session('departure_date'))->format('m');
        $return_day = Carbon::createFromFormat('m-d-Y', session('return_date'))->format('d');
        $return_month = Carbon::createFromFormat('m-d-Y', session('return_date'))->format('m');
        if(session('class') == 'business' || session('class') == 'first'){
            $class = 'B';
        }else{
            $class = 'E';
        }


        //Request to Api
        $url = 'http://api.anywayanyday.com/api/NewRequestRedirect/?Route='.$departure_day.''.$departure_month.''.session('from').''.session('to').''.$return_day.''.$return_month.''.session('to').''.session('from').'&AD='.session('adults').'&CN=0&SC=B&Partner=testapic'.self::$language;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch,CURLOPT_ENCODING,'gzip,deflate');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        $data = curl_exec($ch);
        curl_close($ch);
        $xml = simplexml_load_string($data);

        //Start Search
        $completed = 0;
        while ($completed < 100) {
            $url = 'http://api.anywayanyday.com/api/RequestState/?R='.$xml->attributes()['IdSynonym'];
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch,CURLOPT_ENCODING,'gzip,deflate');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $url);
            $data = curl_exec($ch);
            curl_close($ch);
            $xml_completed = simplexml_load_string($data);
            $completed = ($xml_completed->attributes()['Completed']);
            sleep(2);
        }


        //Info from request
        $url = 'http://api.anywayanyday.com/api/Fares/?V=Matrix&R='.$xml->attributes()['IdSynonym'].'&L=EN&PS=1&PN=1&C=USD';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch,CURLOPT_ENCODING,'gzip,deflate');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        $data = curl_exec($ch);
        curl_close($ch);
        $xml = simplexml_load_string($data);

        $airlines = array();

        foreach ($xml->Arln as $Arln){
            foreach ($Arln->Fare as $Fare){
                $i = 0;
                foreach($Fare->Dir as $Dir){
                    //if($dir->attributes()['DepTm'] != 'N' && $dir->attributes()['ArrTm'] != 'N'){
                    $name = ''.$Arln->attributes()['N'].'';
                    $price = ''.$Fare->attributes()['AT'].'';
                    $from = ''.$Dir->attributes()['DepApt'].'';
                    $to = ''.$Dir->attributes()['ArrApt'].'';
                    //From
                    $airlines[$name][$i]['from'] = $from;
                    $airport_from = Airport::where('code', $from)->first();
                    $airlines[$name][$i]['from_city'] = $airport_from->city->name;
                    $airlines[$name][$i]['dep_time'] = explode(";", ''.$Dir->attributes()['DepTm'].'');
                    //To
                    $airlines[$name][$i]['to'] = $to;
                    $airport_to = Airport::where('code', $to)->first();
                    $airlines[$name][$i]['to_city'] = $airport_to->city->name;
                    $airlines[$name][$i]['arr_time'] = explode(";", ''.$Dir->attributes()['ArrTm'].'');
                    //Time of flight
                    $airlines[$name][$i]['time_of_flight'] = $Dir->attributes()['Hr'].':'.$Dir->attributes()['Min'];
                    //Class
                    $airlines[$name][$i]['class'] = session('class');
                    //Price
                    $airlines[$name][$i]['price'] = $price;
                    $i++;
                    //}
                }
            }
        }

        return view('user.form.step-2-background')->withAirlines($airlines);
    }

}
