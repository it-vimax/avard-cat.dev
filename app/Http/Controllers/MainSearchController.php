<?php

namespace App\Http\Controllers;

use App\Bonus_program;
use Illuminate\Http\Request;
use App\Airport;
use App\City;

class MainSearchController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->q;

        //Airports
        $airports = Airport::where('code', 'like', $search.'%')
            ->orWhereHas('city', function ($query) use ($search){
                $query->where('name', 'like', $search.'%')->orWhere('code', $search);
            })
            ->get();

        //Cities with more than 1 airport
        $cities = City::where('name', 'like', $search.'%')
            ->has('airports', '>', '1')
            ->orWhere('code', $search)
            ->has('airports', '>', '1')
            ->get();

        $airports_arr = $airports->toArray();

        $i = 0;
        //Add cities to airports array
        foreach ($airports as $airport){
            if($airport->city->airports->count() > 1){
                $airports_arr[$i]['sort'] = 8;
            }
            else{
                $airports_arr[$i]['sort'] = 9;
            }
            $airports_arr[$i]['city'] = $airport->city->name.', '.$airport->city->code;
            $airports_arr[$i]['country'] = $airport->city->country->name;
            $i++;
        }

        //Add Cities with more than 1 airport to airports array
        $sort = 0;
        foreach ($cities as $city){
            $airports_arr[$i]['sort'] = $sort++;
            $airports_arr[$i]['id'] = 'city-'.$city->id;
            $airports_arr[$i]['city'] = $city->name.', '.$city->code;
            $airports_arr[$i]['city_id'] = $city->id;
            $airports_arr[$i]['country'] = $city->country->name;
            $airports_arr[$i]['name'] = 'All Airports';
            $airports_arr[$i]['code'] = $city->name;
            $i++;
        }

        return $airports_arr;
    }

    public function bonus_program_search(Request $request)
    {
        $search = $request->q;
        $display_names = Bonus_program::where('display_name', 'like', $search.'%')
                                        ->orWhere('code', 'like', $search.'%')
                                        ->get();

        $display_name_arr = $display_names->toArray();
        return $display_name_arr;
    }
}
