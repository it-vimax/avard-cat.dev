<?php
declare(strict_types=1);
namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function client()
    {
        return $this->hasOne('App\Client', 'user_id');
    }

    public function role()
    {
        return $this->belongsTo('App\Role');
    }

    public function hasAnyRole($roles)
    {
        if(is_array($roles))
        {
            foreach($roles as $role)
            {
                if($this->hasRole($role))
                {
                    return  TRUE;
                }
            }
        }
        else
        {
            if($this->hasRole($roles))
            {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function hasRole($role)
    {
        if($this->role()->where('name', $role)->first())
        {
            return TRUE;
        }
        return FALSE;
    }
}
