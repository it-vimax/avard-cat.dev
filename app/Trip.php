<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    protected $table = 'trips';

    public function client()
    {
        return $this->belongsTo('App\Client');
    }

    public function directions()
    {
        return $this->hasMany('App\Direction');
    }

    public function bonus()
    {
        return $this->hasMany('App\Bonus');
    }
}
