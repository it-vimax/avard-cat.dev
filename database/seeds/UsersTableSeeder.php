<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new User();
        $admin->email = 'admin@admin.com';
        $admin->password = bcrypt('123456');
        $role_admin = Role::where('name', 'Admin')->first();
        $admin->role_id = $role_admin->id;
        $admin->save();
    }
}
