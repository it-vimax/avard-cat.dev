<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(ZoneTableSeeder::class);
        $this->call(CountryTableSeeder::class);
        $this->call(CityAwadSeeder::class);
        $this->call(AirportAwadSeeder::class);
        $this->call(ZonePriceTableSeeder::class);
        $this->call(Bonus_programSeeder::class);
    }
}
