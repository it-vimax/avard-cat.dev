<?php

use Illuminate\Database\Seeder;
use App\Zone;

class ZoneTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $zones = array(
            'Mainland U.S., Alaska & Canada',
            'Hawaii',
            'Mexico',
            'Caribbean',
            'Central America',
            'Northern South America',
            'Southern South America',
            'Europe',
            'Middle East',
            'Northern Africa',
            'Central & Southern Africa',
            'North Asia',
            'Central Asia',
            'South Asia',
            'Japan',
            'Oceania',
            'Australia & New Zealand',
        );

        foreach ($zones as $zone) {
            $new_zone = new Zone();
            $new_zone->name = $zone;
            $new_zone->save();
        }
    }
}
