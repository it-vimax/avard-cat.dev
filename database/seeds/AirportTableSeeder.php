<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\City;

class AirportTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        echo('---------------ADDING AIRPORTS---------------------');
//        $path = storage_path('app/xls/airports.csv');
//        $data = Excel::load($path, function($reader) {
//        })->get();
//
//        if(!empty($data) && $data->count()){
//            foreach ($data as $key => $value) {
//                if(strlen($value->city) && strlen($value->code) && strlen($value->name)){
//                    echo($value->city.'\n');
//                    $city = City::where('name', $value->city)->first();
//                    if(isset($city) && $city && $city->count()){
//                        $insert[] = ['city_id' => $city->id, 'name' => $value->name, 'code' => $value->code];
//                    }
//                }
//            }
//            if(!empty($insert)){
//                DB::table('airports')->insert($insert);
//                echo('Insert Record successfully.');
//            }
//        }

        echo('---------------ADDING AIRPORTS---------------------');
        $path = storage_path('app/xls/airports_new.csv');
        $data = Excel::load($path, function($reader) {
        })->get();

        if(!empty($data) && $data->count()){
            foreach ($data as $key => $value) {
                if(strlen($value->cityname) && strlen($value->code) && strlen($value->name)){
                    echo($value->cityname."\n");
                    $city = City::where('name', $value->cityname)->first();
                    if(isset($city) && $city && $city->count()){
                        $insert[] = ['city_id' => $city->id, 'name' => $value->name, 'code' => $value->code];
                    }
                }
            }
            if(!empty($insert)){
                DB::table('airports')->insert($insert);
                echo('Insert Record successfully.');
            }
        }
    }
}
