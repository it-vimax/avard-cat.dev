<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Country;
use App\City;
use Maatwebsite\Excel\Facades\Excel;

class CityAwadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //New Seeder
        $path = storage_path('app/xls/Cities_awad.xls');
        $data = Excel::load($path, function($reader) {
        })->get();

        foreach ($data as $key => $value) {
            if(strlen($value->nameen) && strlen(trim($value->countrycode))){
                echo($key."=>");
                echo($value->countrycode."\n");
                $country = Country::where('code', $value->countrycode)->first();
                $insert[] = ['name' => $value->nameen, 'code' => $value->code, 'country_id' => $country->id];
            }
        }
        if(!empty($insert)){
            DB::table('cities')->insert($insert);
            echo('Insert Cities successfully.');
        }

    }
}
