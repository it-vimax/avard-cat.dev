<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\City;

class AirportAwadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo('---------------ADDING AIRPORTS---------------------');
        $path = storage_path('app/xls/Airports_awad.xls');
        $data = Excel::load($path, function($reader) {
        })->get();

        if(!empty($data) && $data->count()){
            foreach ($data as $key => $value) {
                if(strlen($value->citycode) && strlen($value->code) && strlen($value->nameen)){
                    echo($value->nameen."\n");
                    $city = City::where('code', $value->citycode)->first();
                    if(isset($city) && $city && $city->count()){
                        $insert[] = ['city_id' => $city->id, 'name' => $value->nameen, 'code' => $value->code];
                    }
                }
            }
            if(!empty($insert)){
                DB::table('airports')->insert($insert);
                echo('Insert Airports successfully.');
            }
        }
    }
}
