<?php

use Illuminate\Database\Seeder;

class Bonus_programSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo("---------------BONUS PROGRAM SEEDER START---------------------\n\n");
        $path = storage_path('app/xls/bonus_programs.xlsx');
        $data = Excel::load($path, function($reader) {
        })->get();

        $insert = [];
        $i=0;
        foreach($data->all() as $item)
        {
            $insert[$i]['code'] = $item->all()['code'];
            $insert[$i]['display_name'] = $item->all()['displayname'];
            $i++;
        }

        if(!empty($insert))
        {
            DB::table('bonus_programs')->insert($insert);
            echo("Insert data of bonus program\n");
        }
    }
}
