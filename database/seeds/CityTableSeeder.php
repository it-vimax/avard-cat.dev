<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Country;
use App\City;
use Maatwebsite\Excel\Facades\Excel;

class CityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

//        $path = storage_path('app/xls/city_country.csv');
//        $data = Excel::load($path, function($reader) {
//        })->get();
//
//        if(!empty($data) && $data->count()){
//            foreach ($data as $key => $value) {
//                if(strlen($value->city) && strlen($value->country)){
//                    echo($value->country.'\n');
//                    $country = Country::where('name', $value->country)->first();
//                    $insert[] = ['name' => $value->city, 'country_id' => $country->id];
//                }
//            }
//            if(!empty($insert)){
//                DB::table('cities')->insert($insert);
//                echo('Insert Record successfully.');
//            }
//            $cities = City::all();
//            foreach ($cities as $city){
//                $find_cities = City::where('name', $city->name)->get();
//                if($find_cities->count() > 1){
//                    $i = 1;
//                    foreach ($find_cities as $find_city){
//                        if($i>1){
//                            $find_city->delete();
//                        }
//                        $i++;
//                        if($find_cities->count() == $i){
//                            $i = 1;
//                        }
//
//                    }
//                }
//            }
//        }

        //New Seeder
        $path = storage_path('app/xls/airports_new.csv');
        $data = Excel::load($path, function($reader) {
        })->get();

        foreach ($data as $key => $value) {
            if(strlen($value->cityname) && strlen($value->countryname)){
                echo($value->countryname."\n");
                $country = Country::where('name', $value->countryname)->first();
                $insert[] = ['name' => $value->cityname, 'country_id' => $country->id];
            }
        }
        if(!empty($insert)){
            DB::table('cities')->insert($insert);
            echo('Insert Record successfully.');
        }

        $cities = City::all();
        foreach ($cities as $city){
            $find_cities = City::where('name', $city->name)->get();
            if($find_cities->count() > 1){
                $i = 1;
                foreach ($find_cities as $find_city){
                    if($i>1){
                        $find_city->delete();
                    }
                    $i++;
                    if($find_cities->count() == $i){
                        $i = 1;
                    }

                }
            }
        }

    }
}
