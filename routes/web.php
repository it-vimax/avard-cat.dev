<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

//Testing AwarWallet API
Route::get('/auth', 'TestController@auth');
Route::get('/test', 'TestController@test');

Route::get('/', 'HomeController@index')->name('home');

// SEARCH
Route::post('/form-search', 'MainSearchController@index')->name('main.search');
Route::post('/form-search/bonus-program', 'MainSearchController@bonus_program_search')->name('bonus_program.search');

//Step 2
Route::post('/round-trip-step-two', 'FormController@roundTripStepTwo')->name('roundTripStepTwo');
Route::post('/one-way-step-two', 'FormController@oneWayStepTwo')->name('oneWayStepTwo');
Route::post('/multi-city-step-two', 'FormController@multiCityStepTwo')->name('multiCityStepTwo');
Route::post('/step-two-background', 'FormController@stepTwoBackground')->name('stepTwoBackground');

//Step 3
Route::match(['get', 'post'], '/step-three', 'FormController@stepThree')->name('stepThree');

//Step 4
Route::post('/step-four', 'FormController@stepFour')->name('stepFour');

//Step 5 - Registration
Route::match(['get', 'post'], '/enter', 'PaymentController@enter')->name('enter');
Route::post('/registration', 'PaymentController@registration')->name('registration');


Route::get('/home', 'HomeController@role');

//Admin Routes
Route::group(['middleware'=>'roles', 'roles'=> ['admin'], 'prefix' => 'admin'], function(){
    Route::get('/', 'Admin\AdminController@index')->name('admin.index');
    Route::get('/trips', 'Admin\TripController@index')->name('admin.trips');
    Route::get('/trip/{id}', 'Admin\TripController@show');
    Route::post('/trip/update/status', 'Admin\TripController@update_status')->name('admin.trip.update.status');
});

//AwardWallet
Route::get('/awardwallet/auth', 'AwardWalletController@auth')->name('awardwallet.auth');
Route::get('/awardwallet/callback', 'AwardWalletController@callback')->name('awardwallet.callback');